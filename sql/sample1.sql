INSERT INTO city (
       name,
       state,
       country
)
VALUES (
       'New York',
       'New York',
       'United States'
);

INSERT INTO city (
       name,
       state,
       country
)
VALUES (
       'Seattle',
       'Washington',
       'United States'
);

INSERT INTO publisher (
       name,
       city_id,
       notes
)
VALUES (
       'Viking',
       (SELECT city_id
       FROM city
       WHERE city.name = 'New York' AND city.state = 'New York'),
       'Part of Penguin Group'
);

INSERT INTO book (
       title,
       dewey,
       copyright_date
)
VALUES (
       'Road Ahead, The',
       '004.67',
       1995
);

INSERT INTO physical_book (
       isbn,
       edition,
       printing,
       price,
       number_pages,
       length,
       width,
       height,
       format_id,
       location_id,
       publisher_id,
       publication_city_id,
       book_id,
       notes
)
VALUES (
       '0-670-77289-5',
       1,
       1,
       29.95,
       286,
       9.25,
       6.5,
       1.25,
       (SELECT format.format_id
       FROM format
       WHERE format.name = 'Hardcover'),
       (SELECT location.location_id
       FROM location
       WHERE location.name = 'Library'),
       (SELECT publisher.publisher_id
       FROM publisher
       WHERE publisher.name = 'Viking'),
       (SELECT city_id
       FROM city
       WHERE city.name = 'New York' AND city.state = 'New York'),
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       'Companion Interactive CD-ROM Inside'
);

INSERT INTO suffix (name) VALUES ('III');

INSERT INTO nationality (name) VALUES ('American');

INSERT INTO person (
       last_name,
       middle_name,
       first_name,
       suffix_id,
       birth_year,
       birth_date,
       occupation,
       nationality_id,
       residence_id,
       birth_city_id
)
VALUES (
       'Gates',
       'Henry',
       'William',
       (SELECT suffix.suffix_id
       FROM suffix
       WHERE suffix.name = 'III'),
       1955,
       '10/28/1955',
       'Chairman and Chief Software Architect, Microsoft Corporation',
       (SELECT nationality.nationality_id
       FROM nationality
       WHERE nationality.name = 'American'),
       (SELECT city.city_id
       FROM city
       WHERE city.name = 'Seattle' AND city.state = 'Washington'),
       (SELECT city.city_id
       FROM city
       WHERE city.name = 'Seattle' AND city.state = 'Washington')
);

INSERT INTO person (
       last_name,
       first_name,
       honorifics_id,
       occupation,
       nationality_id
)
VALUES (
       'Myhrvold',
       'Nathan',
       (SELECT honorifics.honorifics_id
       FROM honorifics
       WHERE honorifics.name = 'Ph. D.'),
       'Founder of Intellectual Ventures',
       (SELECT nationality.nationality_id
       FROM nationality
       WHERE nationality.name = 'American')
);

INSERT INTO person (
       last_name,
       first_name,
       birth_year,
       occupation
)
VALUES (
       'Rinearson',
       'Peter',
       1954,
       'Journalist'
);

INSERT INTO subject (name) VALUES ('Information superhighway -- United States');
INSERT INTO subject (name) VALUES ('Information technology -- United States');
INSERT INTO subject (name) VALUES ('Computer networks -- United States');
INSERT INTO subject (name) VALUES ('Telecommunications -- United States');
INSERT INTO subject (name) VALUES ('Computer industry -- United States');

INSERT INTO book_person_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Gates' AND person.first_name = 'William'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Author'),
       TRUE
);

INSERT INTO book_person_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Myhrvold' AND person.first_name = 'Nathan'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Author'),
       FALSE
);

INSERT INTO book_person_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Rinearson' AND person.first_name = 'Peter'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Author'),
       FALSE
);

INSERT INTO book_subject_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       (SELECT subject.subject_id
       FROM subject
       WHERE subject.name = 'Information superhighway -- United States')
);

INSERT INTO book_subject_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       (SELECT subject.subject_id
       FROM subject
       WHERE subject.name = 'Information technology -- United States')
);

INSERT INTO book_subject_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       (SELECT subject.subject_id
       FROM subject
       WHERE subject.name = 'Computer networks -- United States')
);

INSERT INTO book_subject_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       (SELECT subject.subject_id
       FROM subject
       WHERE subject.name = 'Telecommunications -- United States')
);

INSERT INTO book_subject_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'Road Ahead, The'),
       (SELECT subject.subject_id
       FROM subject
       WHERE subject.name = 'Computer industry -- United States')
);

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'Rippon',
       'Laurie'
);

INSERT INTO jobs (name) VALUES ('Jacket Designer');

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id 
				     FROM book
				     WHERE book.title = 'Road Ahead, The'
				     )
       ),			     
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Rippon' AND person.first_name = 'Laurie'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Jacket Designer')
);

INSERT INTO city (
       name,
       state,
       country
)
VALUES (
       'Westbury',
       'Connecticut',
       'United States'
);


INSERT INTO person (
       last_name,
       first_name,
       birth_year,
       birth_date,
       birth_city_id,
       nationality_id,
       occupation
)
VALUES (
       'Leibovitz',
       'Annie',
       1949,
       '10/2/1949',
       (SELECT city_id
       FROM city
       WHERE city.name = 'Westbury'),
       (SELECT nationality_id
       FROM nationality
       WHERE nationality.name = 'American'),
       'Photographer'
);

INSERT INTO jobs (name) VALUES ('Jacket Photographer');

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'Road Ahead, The'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Leibovitz' AND person.first_name = 'Annie'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Jacket Photographer')
);