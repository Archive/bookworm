-- DROP SEQUENCE series_seq;
-- DROP TABLE series;
-- DROP SEQUENCE book_seq;
-- DROP TABLE book;
-- DROP SEQUENCE location_seq;
-- DROP TABLE location;
-- DROP SEQUENCE city_seq;
-- DROP TABLE city;
-- DROP SEQUENCE publisher_seq;
-- DROP TABLE publisher;
-- DROP SEQUENCE bookstore_topics_seq;
-- DROP TABLE bookstore_topics;
-- DROP SEQUENCE physical_book_seq;
-- DROP TABLE physical_book;
-- DROP SEQUENCE audio_book_seq;
-- DROP TABLE audio_book;
-- DROP SEQUENCE ebook_seq;
-- DROP TABLE e_book;
-- DROP SEQUENCE person_seq;
-- DROP TABLE person;
-- DROP SEQUENCE subject_seq;
-- DROP TABLE subject;
-- DROP SEQUENCE jobs_seq;
-- DROP TABLE jobs;
-- DROP TABLE book_person_link;
-- DROP TABLE book_subject_link;

CREATE TABLE series (
       series_id SERIAL PRIMARY KEY,
       name VARCHAR,
       length INTEGER,
       notes TEXT
);

CREATE TABLE city (
       city_id SERIAL PRIMARY KEY,
       name VARCHAR,
       state VARCHAR,
       country VARCHAR,
       UNIQUE (name, state, country)
);

CREATE TABLE publisher (
       publisher_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE,
       city_id INTEGER REFERENCES city,
       notes TEXT
);

CREATE TABLE publisher_series (
       publisher_series_id SERIAL PRIMARY KEY,
       name VARCHAR,
       publisher_id INTEGER REFERENCES publisher,
       notes TEXT
);

CREATE TABLE languages (
       language_id CHAR(5) PRIMARY KEY,
       name VARCHAR UNIQUE
);

CREATE TABLE book (
       book_id SERIAL PRIMARY KEY,
       title VARCHAR,
       subtitle VARCHAR,
       alternate_names VARCHAR,
       dewey VARCHAR(15),
       copyright_date INTEGER,
       original_language_id CHAR(5) REFERENCES languages DEFAULT 'eng',
       series_id INTEGER REFERENCES series,
       series_location INTEGER,
       biography BOOLEAN DEFAULT FALSE,
       fiction BOOLEAN DEFAULT FALSE,
       notes TEXT
);

CREATE TABLE location (
       location_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE
);


CREATE TABLE bookstore_topics (
       bookstore_topics_id SERIAL PRIMARY KEY,
       topic VARCHAR UNIQUE
);

CREATE TABLE format (
       format_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE
);

CREATE TABLE currency (
       currency_id SERIAL PRIMARY KEY,
       name VARCHAR
);

CREATE TABLE type (
       type_id SERIAL PRIMARY KEY,
       name VARCHAR
);

CREATE TABLE physical_book (
       physical_book_id SERIAL PRIMARY KEY,
       isbn VARCHAR (15),
       edition INTEGER,
       printing	INTEGER,
       price REAL,
       currency_id INTEGER REFERENCES currency DEFAULT 1,
       autographed BOOLEAN DEFAULT FALSE,
       number_pages INTEGER,
       length REAL,
       width REAL,
       height REAL,
       date_acquired DATE,
       language_id CHAR (5) REFERENCES languages DEFAULT 'eng',
       format_id INTEGER REFERENCES format,
       location_id INTEGER REFERENCES location,
       publisher_id INTEGER REFERENCES publisher,
       publication_city_id INTEGER REFERENCES city,
       book_id INTEGER REFERENCES book NOT NULL,
       bookstore_topics_id INTEGER REFERENCES bookstore_topics,
       type_id INTEGER REFERENCES type,
       publisher_series_id INTEGER REFERENCES publisher_series,
       notes TEXT
);

CREATE TABLE audio_book (
       audio_book_id SERIAL PRIMARY KEY,
       price VARCHAR,
       duration INTEGER,
       size INTEGER,
       date_acquired DATE,
       language_id CHAR (5) REFERENCES languages DEFAULT 'eng',
       format_id INTEGER REFERENCES format,
       location_id INTEGER REFERENCES location,
       publisher_id INTEGER REFERENCES publisher,
       book_id INTEGER REFERENCES book NOT NULL,
       bookstore_topics_id INTEGER REFERENCES bookstore_topics,
       notes TEXT
);

CREATE TABLE e_book (
       e_book_id SERIAL PRIMARY KEY,
       price VARCHAR,
       language_id CHAR (5) REFERENCES languages DEFAULT 'eng',
       location_id INTEGER REFERENCES location,
       publisher_id INTEGER REFERENCES publisher,
       book_id INTEGER REFERENCES book NOT NULL,
       notes TEXT
);

CREATE TABLE honorifics (
       honorifics_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE
);

CREATE TABLE personal_title (
       personal_title_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE
);

CREATE TABLE suffix (
       suffix_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE
);

CREATE TABLE nationality (
       nationality_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE
);

CREATE TABLE person (
       person_id SERIAL PRIMARY KEY,
       last_name VARCHAR,
       middle_name VARCHAR,
       first_name VARCHAR,
       suffix_id INTEGER REFERENCES suffix, 
       honorifics_id INTEGER REFERENCES honorifics,
       personal_title_id INTEGER REFERENCES personal_title,
       birth_year INTEGER,
       birth_date DATE,
       death_year INTEGER,
       death_date DATE,
       nationality_id INTEGER REFERENCES nationality,
       occupation VARCHAR,
       residence_id INTEGER REFERENCES city,
       birth_city_id INTEGER REFERENCES city,
       notes TEXT
);

CREATE TABLE subject (
       subject_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE
);

CREATE TABLE jobs (
       jobs_id SERIAL PRIMARY KEY,
       name VARCHAR UNIQUE
);

CREATE TABLE book_person_link (
       book_id INTEGER REFERENCES book NOT NULL,
       person_id INTEGER REFERENCES person NOT NULL,
       jobs_id INTEGER REFERENCES jobs NOT NULL,
       first boolean 
);

CREATE TABLE book_subject_link (
       book_id INTEGER REFERENCES book NOT NULL,
       subject_id INTEGER REFERENCES subject NOT NULL
);

CREATE TABLE physicalbook_person_link (
       physical_book_id INTEGER REFERENCES physical_book NOT NULL,
       person_id INTEGER REFERENCES person NOT NULL,
       jobs_id INTEGER REFERENCES jobs NOT NULL
);

CREATE TABLE audiobook_person_link (
       audio_book_id INTEGER REFERENCES audio_book NOT NULL,
       person_id INTEGER REFERENCES person NOT NULL,
       jobs_id INTEGER REFERENCES jobs NOT NULL
);
       
CREATE TABLE ebook_person_link (
       e_book_id INTEGER REFERENCES e_book NOT NULL,
       person_id INTEGER REFERENCES person NOT NULL,
       jobs_id INTEGER REFERENCES jobs NOT NULL
);
