Insert INTO languages VALUES (
       'eng',
       'English'
);

INSERT INTO languages VALUES (
       'ara',
       'Arabic'
);

INSERT INTO languages VALUES (
       'arc',
       'Aramaic'
);

INSERT INTO languages VALUES (
       'dan',
       'Danish'
);

INSERT INTO languages VALUES (
       'chi',
       'Chinese'
);

INSERT INTO languages VALUES (
       'cze',
       'Czech'
);

INSERT INTO languages VALUES (
       'fre',
       'French'
);

INSERT INTO languages VALUES (
       'ger',
       'German'
);

INSERT INTO languages VALUES (
       'grc',
       'Greek, Ancient'
);

INSERT INTO languages VALUES (
       'gre',
       'Greek'
);

INSERT INTO languages VALUES (
       'heb',
       'Hebrew'
);

INSERT INTO languages VALUES (
       'hin',
       'Hindi'
);

INSERT INTO languages VALUES (
       'hun',
       'Hungarian'
);
 
INSERT INTO languages VALUES (
       'ita',
       'Italian'
);

INSERT INTO languages VALUES (
       'jpn',
       'Japanese'
);

INSERT INTO languages VALUES (
       'kor',
       'Korean'
);

INSERT INTO languages VALUES (
       'lat',
       'Latin'
);

INSERT INTO languages VALUES (
       'myn',
       'Mayan languages'
);

INSERT INTO languages VALUES (
       'nor',
       'Norwegian'
);

INSERT INTO languages VALUES (
       'por',
       'Portuguese'
);

INSERT INTO languages VALUES (
       'rus',
       'Russian'
);

INSERT INTO languages VALUES (
       'spa',
       'Spanish; Castillian'
);

INSERT INTO languages VALUES (
       'swe',
       'Swedish'
);

INSERT INTO languages VALUES (
       'wel',
       'Welsh'
);