INSERT INTO city (country) VALUES ('Germany');

INSERT INTO city (
       name,
       country
)
VALUES (
       'Mexico City',
       'Mexico'
);

INSERT INTO city (
       state,
       country
)
VALUES (
       'Perth',
       'Australia'
);

INSERT INTO city (
       name,
       state,
       country
)
VALUES (
       'Berkeley',
       'California',
       'United States'
);

INSERT INTO book (
       title,
       dewey,
       copyright_date
)
VALUES (
       'GTK+ / GNOME Application Development',
       '005.133',
       1999
);

INSERT INTO bookstore_topics (topic) VALUES ('GNU / Linux / UNIX Development');

INSERT INTO physical_book (
       isbn,
       edition,
       printing,
       price,
       autographed,
       number_pages,
       length,
       width,
       height,
       format_id,
       location_id,
       publisher_id,
       publication_city_id,
       book_id,
       bookstore_topics_id
)
VALUES (
       '0-7357-0078-8',
       1,
       1,
       '$39.99',
       TRUE,
       492,
       9.02,
       7,
       1.25,
       (SELECT format.format_id
       FROM format
       WHERE format.name = 'Softcover'),
       (SELECT location.location_id
       FROM location
       WHERE location.name = 'Library'),
       (SELECT publisher.publisher_id
       FROM publisher
       WHERE publisher.name = 'New Riders'),
       (SELECT city_id
       FROM city
       WHERE city.name = 'Indianapolis'),
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'GTK+ / GNOME Application Development'),       
       (SELECT bookstore_topics.bookstore_topics_id
       FROM bookstore_topics
       WHERE bookstore_topics.topic = 'GNU / Linux / UNIX Development')
);

INSERT INTO person (
       last_name,
       first_name,
       nationality_id,
       occupation,
       residence_id
)
VALUES (
       'Pennington',
       'Havoc',
       (SELECT nationality.nationality_id
       FROM nationality
       WHERE nationality.name = 'American'),	       
       'Developer at Red Hat, Inc.',
       (SELECT city.city_id
       FROM city
       WHERE city.name = 'Chapel Hill')
);

INSERT INTO book_person_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'GTK+ / GNOME Application Development'),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Pennington' AND person.first_name = 'Havoc'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Author'),
       TRUE
);

INSERT INTO nationality (name) VALUES ('Mexican');

INSERT INTO person (
       last_name,
       first_name,
       nationality_id,
       occupation,
       residence_id
)
VALUES (
       'de Icaza',
       'Miguel',
       (SELECT nationality_id
       FROM nationality
       WHERE nationality.name = 'Mexican'),
       'Founder of Ximian',
       (SELECT city_id
       FROM city
       WHERE city.name = 'Cambridge' AND city.state = 'Massachusetts')
);
       
INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'de Icaza' AND person.first_name = 'Miguel'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Foreward Author')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Dwyer' AND person.first_name = 'David'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Publisher')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Petrycki' AND person.first_name = 'Laurie'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Executive Editor')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Purdum' AND person.first_name = 'Katie'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Acquisitions Editor')
);

INSERT INTO jobs (name) VALUES ('Development Editor');

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'Chalex',
       'Jim'
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Chalex' AND person.first_name = 'Jim'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Development Editor')
);

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'Brown',
       'Gina'
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Brown' AND person.first_name = 'Gina'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Managing Editor')
);

INSERT INTO jobs (name) VALUES ('Project Editor');

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'Loveall',
       'Laura'
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Loveall' AND person.first_name = 'Laura'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Project Editor')
);

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'McCarty',
       'Clint'
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'McCarty' AND person.first_name = 'Clint'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Project Editor')
);

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'Johnson',
       'Gayle'
);

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'McFarland',
       'Audra'
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Johnson' AND person.first_name = 'Gayle'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Copy Editor')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'McFarland' AND person.first_name = 'Audra'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Copy Editor')
);

INSERT INTO person (
       last_name,
       middle_name,
       first_name
)
VALUES (
       'Lee',
       'Dean',
       'Joy'
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Lee' AND person.first_name = 'Joy'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Indexer')
);

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'Garzik',
       'Jeff'
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Garzik' AND person.first_name = 'Jeff'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Technical Reviewer')
);
       
INSERT INTO person (
       last_name,
       first_name,
       residence_id
)
VALUES (
       'Henstridge',
       'James',
       (SELECT city_id
       FROM city
       WHERE city.state = 'Perth')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Henstridge' AND person.first_name = 'James'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Technical Reviewer')
);

INSERT INTO person (
       last_name,
       first_name,
       residence_id
)
VALUES (
       'Levien',
       'Raph',
       (SELECT city_id
       FROM city
       WHERE city.name = 'Berkeley')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Levien' AND person.first_name = 'Raph'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Technical Reviewer')
);

INSERT INTO person (
       last_name,
       first_name,
       birth_year,
       birth_city_id,
       residence_id
)
VALUES (
       'Mena Quintero',
       'Federico',
       1976,
       (SELECT city_id
       FROM city
       WHERE city.name = 'Mexico City'),
       (SELECT city_id
       FROM city
       WHERE city.name = 'Mexico City')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Mena Quintero' AND person.first_name = 'Federico'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Technical Reviewer')
);

INSERT INTO person (
       last_name,
       first_name,
       birth_year,
       birth_city_id
)
VALUES (
       'Vollmer',
       'Marius',
       1971,
       (SELECT city_id
       FROM city
       WHERE city.country = 'Germany')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Vollmer' AND person.first_name = 'Marius'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Technical Reviewer')
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),				     
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Loveall' AND person.first_name = 'Laura'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Proofreader')
);

INSERT INTO person (
       last_name,
       first_name
)
VALUES (
       'Parker',
       'Amy'
);

INSERT INTO physicalbook_person_link VALUES (
       (SELECT physical_book.physical_book_id
       FROM physical_book
       WHERE physical_book.book_id = (
				     SELECT book.book_id
				     FROM book
				     WHERE book.title = 'GTK+ / GNOME Application Development'
				     )
       ),
       (SELECT person.person_id
       FROM person
       WHERE person.last_name = 'Parker' AND person.first_name = 'Amy'),
       (SELECT jobs.jobs_id
       FROM jobs
       WHERE jobs.name = 'Layout Technician')
);

INSERT INTO book_subject_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'GTK+ / GNOME Application Development'),
       (SELECT subject.subject_id
       FROM subject
       WHERE subject.name = 'Application software -- Development')
);

INSERT INTO subject (name) VALUES ('Graphical user interfaces (Computer systems)');

INSERT INTO book_subject_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'GTK+ / GNOME Application Development'),
       (SELECT subject.subject_id
       FROM subject
       WHERE subject.name = 'Graphical user interfaces (Computer systems)')
);

INSERT INTO subject (name) VALUES ('Computer games -- Programming');

INSERT INTO book_subject_link VALUES (
       (SELECT book.book_id
       FROM book
       WHERE book.title = 'GTK+ / GNOME Application Development'),
       (SELECT subject.subject_id
       FROM subject
       WHERE subject.name = 'Computer games -- Programming')
);
