INSERT INTO currency (name) VALUES ('USD');  -- US Dollar
INSERT INTO currency (name) VALUES ('GBP');  -- Great Britain Pound
INSERT INTO currency (name) VALUES ('AUD');  -- Australia Dollar
INSERT INTO currency (name) VALUES ('CAD');  -- Canada Dollar
INSERT INTO currency (name) VALUES ('CNY');  -- China Yuan Renminbi
INSERT INTO currency (name) VALUES ('EUR');  -- Euro
INSERT INTO currency (name) VALUES ('HKD');  -- Hong Kong Dollar
INSERT INTO currency (name) VALUES ('JPY');  -- Japan Yen
INSERT INTO currency (name) VALUES ('DEM');  -- Germay Deutsche Mark
INSERT INTO currency (name) VALUES ('FRF');  -- France Franc
INSERT INTO currency (name) VALUES ('ITL');  -- Italy Lire
