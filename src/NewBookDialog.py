#!/usr/bin/python

import gtk
import gtk.glade
import Library
import util
import gnome.ui
import time
import gobject
import xml.sax.saxutils
import isbn

from gettext import gettext as _
from TestError import *

from CityDialog import CityWidget
from OptionData import LanguageWidget
from OptionData import FormatWidget
from OptionData import CurrencyWidget
from OptionData import TypeWidget
from OptionData import LocationWidget
from OptionData import BookstoreTopicsWidget

from NotesWidget import NotesWidget
from PublisherDialog import PublisherModel
from PublisherSeriesWidget import PublisherSeriesWidget

from Subjects import SubjectsModel, SubjectsAddEntry
from PersonJobList import PersonJobList
from SeriesWidget import SeriesWidget
class NewBookDialog:
    BOOK_PEOPLE_COLUMN_JOB = 0
    BOOK_PEOPLE_COLUMN_NAME = 1

    def __init__ (self, library):
        self.library = library
        self.xml = gtk.glade.XML ('NewBookDialog.glade')
        self.initialize_book_tab ()
        self.initialize_physical_tab ()
        self.initialize_book_location ()

    def run (self):
        dialog = self.xml.get_widget ('new_book_dialog')
        dialog.maximize ()
        dialog.show ()

        while 1:
            button = dialog.run ()
            if button == gtk.RESPONSE_OK:
                try:
                    self.test ()
                except TestError, msg:
                    print msg
                    continue
                book = self.dehydrate ()
                dialog.destroy ()
                return book
            else:
                dialog.destroy ()
                return None

    def test (self):
        isbn_text = self.xml.get_widget ('isbn_entry').get_text ()
        if isbn_text:
            try:
                i = isbn.isbn (isbn_text)
                i.validate ()
            except 'InvalidISBN':
                raise TestError ("ISBN is invalid")
        dewey_text = self.xml.get_widget ('book_dewey_entry').get_text ()
        if dewey_text and not util.test_dewey (dewey_text):
            raise TestError ("Dewey is invalid")
        edition_text = self.xml.get_widget ('physical_edition_entry').get_text ()
        if edition_text and not util.test_int (edition_text):
            raise TestError ("Edition is invalid")
        printing_text = self.xml.get_widget ('physical_printing_entry').get_text ()
        if printing_text and not util.test_int (printing_text):
            raise TestError ("Printing is invalid")
        cost_text = self.xml.get_widget ('physical_cost_entry').get_text ()
        if cost_text and not util.test_float (cost_text):
            raise TestError ("Cost is invalid")
        page_count_text = self.xml.get_widget ('physical_page_count_entry').get_text ()
        if page_count_text and not util.test_int (page_count_text):
            raise TestError ("Page Count is invalid")
        length_text = self.xml.get_widget ('physical_length_entry').get_text ()
        if length_text and not util.test_float (length_text):
            raise TestError ("Length is invalid")
        width_text = self.xml.get_widget ('physical_width_entry').get_text ()
        if width_text and not util.test_float (width_text):
            raise TestError ("Width is invalid")
        height_text = self.xml.get_widget ('physical_height_entry').get_text ()
        if height_text and not util.test_float (height_text):
            raise TestError ("Height is invalid")

    def initialize_book_tab (self):
        self.xml.get_widget ('isbn_entry').connect ('insert-text', util.isbn_entry_insert_text_cb)
        self.xml.get_widget ('book_title_entry').connect ('changed', self.book_title_entry_changed)
        self.subjects_model = SubjectsModel (self.library)
        tree_view = self.xml.get_widget ('book_subject_tree_view')
        tree_view.set_model (self.subjects_model)
        tree_view.set_search_column (SubjectsModel.SUBJECT_COLUMN_NAME)
        cell = gtk.CellRendererToggle()
        def cell_toggled (cell, path_str, model):
            iter = model.get_iter (path_str)
            toggled = model.get_value (iter, SubjectsModel.SUBJECT_COLUMN_SELECTED)
            model.set (iter, 1, not toggled)
        cell.connect ('toggled', cell_toggled, self.subjects_model)
        column = gtk.TreeViewColumn (None)
        tree_view.append_column (column)
        column.pack_start (cell, False)
        column.set_attributes (cell, active=SubjectsModel.SUBJECT_COLUMN_SELECTED)
        cell = gtk.CellRendererText()
        column.pack_start (cell, True)
        column.set_attributes (cell, text=SubjectsModel.SUBJECT_COLUMN_NAME)
        self.xml.get_widget ('book_dewey_entry').connect ('insert-text', util.dewey_entry_insert_text_cb)
        self.xml.get_widget ('book_copyright_date_entry').connect ('insert-text', util.date_entry_insert_text_cb)

        self.original_language = LanguageWidget (self.library, self.xml.get_widget ('book_original_language_omenu'))
        SubjectsAddEntry (self.xml.get_widget ('book_subject_entry'),
                          self.xml.get_widget ('book_subject_button'),
                          tree_view)

        tree_view = self.xml.get_widget ('book_people_job_tree_view')
        self.person_job_list = PersonJobList (self.library)
        tree_view.set_model (self.person_job_list)
        self.person_job_list.add_buttons (tree_view,
                                          self.xml.get_widget ('book_people_job_add_button'),
                                          self.xml.get_widget ('book_people_job_edit_button'),
                                          self.xml.get_widget ('book_people_job_delete_button'))
        cell = gtk.CellRendererToggle ()
        def primary_toggled (cell, path_str, model):
            iter = model.get_iter (path_str)
            toggled = model.get_value (iter, PersonJobList.PERSON_JOB_COLUMN_PRIMARY)
            model.set (iter, PersonJobList.PERSON_JOB_COLUMN_PRIMARY, not toggled)
        cell.connect ('toggled', primary_toggled, self.person_job_list)
        column = gtk.TreeViewColumn (_("Primary"), cell,
                                     active=PersonJobList.PERSON_JOB_COLUMN_PRIMARY)
        tree_view.append_column (column)
        column = gtk.TreeViewColumn (_("Job"), gtk.CellRendererText (),
                                     text=PersonJobList.PERSON_JOB_COLUMN_JOB_DESCRIPTION)
        tree_view.append_column (column)
        column = gtk.TreeViewColumn (_("Person"), gtk.CellRendererText (),
                                     text=PersonJobList.PERSON_JOB_COLUMN_PERSON_NAME)
        tree_view.append_column (column)
        self.book_notes = NotesWidget (self.xml.get_widget ('book_notes_button'))
        self.series = SeriesWidget (self.library,
                                    self.xml.get_widget ('book_series_name_label'),
                                    self.xml.get_widget ('book_series_location_spin'),
                                    self.xml.get_widget ('book_series_change_button'))

    def initialize_physical_tab (self):
        dialog = self.xml.get_widget ('new_book_dialog')
        self.type = TypeWidget (self.library,
                                self.xml.get_widget ('physical_book_type_omenu'),
                                self.xml.get_widget ('physical_book_type_new_button'))

        self.xml.get_widget ('physical_edition_entry').connect ('insert-text', util.int_entry_insert_text_cb)
        self.xml.get_widget ('physical_printing_entry').connect ('insert-text', util.int_entry_insert_text_cb)
        self.xml.get_widget ('physical_cost_entry').connect ('insert-text', util.float_entry_insert_text_cb)
        self.xml.get_widget ('physical_page_count_entry').connect ('insert-text', util.int_entry_insert_text_cb)
        self.xml.get_widget ('physical_date_acquired_entry').connect ('insert-text', util.date_entry_insert_text_cb)
        self.xml.get_widget ('physical_length_entry').connect ('insert-text', util.float_entry_insert_text_cb)
        self.xml.get_widget ('physical_width_entry').connect ('insert-text', util.float_entry_insert_text_cb)
        self.xml.get_widget ('physical_height_entry').connect ('insert-text', util.float_entry_insert_text_cb)
        self.publication_city = CityWidget (self.library,
                                            self.xml.get_widget ('physical_publishing_city_label'),
                                            self.xml.get_widget ('physical_publishing_city_button'),
                                            parent=dialog)
        self.language = LanguageWidget (self.library, self.xml.get_widget ('physical_language_omenu'))
        self.format = FormatWidget (self.library, self.xml.get_widget ('physical_book_format_omenu'))
        self.currency = CurrencyWidget (self.library, self.xml.get_widget ('physical_currency_omenu'))
        tree_view = self.xml.get_widget ('physical_book_publisher_tree_view')
        self.physical_publisher_list = PublisherModel (self.library)
        self.physical_publisher_list.add_buttons (tree_view,
                                                  self.xml.get_widget ('physical_new_publisher_button'))
        tree_view.set_model (self.physical_publisher_list)
        column = gtk.TreeViewColumn ('', gtk.CellRendererText (),
                                     text=PublisherModel.PUBLISHER_COLUMN_NAME)
        tree_view.append_column (column)
        
        tree_view = self.xml.get_widget ('physical_book_people_job_tree_view')
        self.physical_person_job_list = PersonJobList (self.library)
        tree_view.set_model (self.physical_person_job_list)
        self.physical_person_job_list.add_buttons (tree_view,
                                          self.xml.get_widget ('physical_book_people_job_add_button'),
                                          self.xml.get_widget ('physical_book_people_job_edit_button'),
                                          self.xml.get_widget ('physical_book_people_job_delete_button'))
        column = gtk.TreeViewColumn (_("Job"), gtk.CellRendererText (),
                                     text=PersonJobList.PERSON_JOB_COLUMN_JOB_DESCRIPTION)
        tree_view.append_column (column)
        column = gtk.TreeViewColumn (_("Person"), gtk.CellRendererText (),
                                     text=PersonJobList.PERSON_JOB_COLUMN_PERSON_NAME)
        tree_view.append_column (column)
        self.physical_book_notes = NotesWidget (self.xml.get_widget ('physical_book_notes_button'))
        self.publisher_series = PublisherSeriesWidget (self.library,
                                                       self.xml.get_widget ('physical_publisher_series_label'),
                                                       self.xml.get_widget ('physical_publisher_series_change_button'))

    def initialize_book_location (self):
        self.bookstore_topics = BookstoreTopicsWidget (self.library,
                                                       self.xml.get_widget ('book_location_bookstore_topics_omenu'),
                                                       self.xml.get_widget ('book_location_bookstore_topics_new_button'))
        self.location = LocationWidget (self.library,
                                        self.xml.get_widget ('book_location_location_omenu'),
                                        self.xml.get_widget ('book_location_location_new_button'))

    def book_title_entry_changed (self, entry):
        label = self.xml.get_widget ('title_label')
        label.set_markup ('<span size="large" weight="bold">%s</span>' % xml.sax.saxutils.escape (entry.get_text()))


    def dehydrate (self, physical_book=None):
        isbn = self.xml.get_widget ('isbn_entry').get_text ()
        title = self.xml.get_widget ('book_title_entry').get_text ()
        subtitle = self.xml.get_widget ('book_subtitle_entry').get_text ()
        alternate_names = self.xml.get_widget ('book_altenate_title_entry').get_text ()
        dewey = self.xml.get_widget ('book_dewey_entry').get_text ()
        copyright_date = self.xml.get_widget ('book_copyright_date_entry').get_text ()
        fiction = self.xml.get_widget ('book_fiction_toggle').get_active ()
        biography = self.xml.get_widget ('book_biography_toggle').get_active ()
        edition = self.xml.get_widget ('physical_edition_entry').get_text ()
        printing = self.xml.get_widget ('physical_printing_entry').get_text ()
        price = self.xml.get_widget ('physical_cost_entry').get_text ()
        date_acquired = self.xml.get_widget ('physical_date_acquired_entry').get_text ()
        number_pages = self.xml.get_widget ('physical_page_count_entry').get_text ()
        length = self.xml.get_widget ('physical_length_entry').get_text ()
        width = self.xml.get_widget ('physical_width_entry').get_text ()
        height = self.xml.get_widget ('physical_height_entry').get_text ()
        autographed = self.xml.get_widget ('physical_autographed_toggle').get_active ()
        oversized = self.xml.get_widget ('physical_oversized_toggle').get_active ()
        language = self.language.get_object ()
        format = self.format.get_object ()
        currency = self.currency.get_object ()
        publisher = self.physical_publisher_list.get_object ()
        publication_city = self.publication_city.get_object ()
        original_language = self.original_language.get_object ()
        bookstore_topics = self.bookstore_topics.get_object ()
        location = self.location.get_object ()
        type = self.type.get_object ()
        book_notes = self.book_notes.get_object () 
        physical_book_notes = self.physical_book_notes.get_object ()
        series = self.series.get_object ()
        publisher_series = self.publisher_series.get_object ()

        if series:
            series_location = int (self.xml.get_widget ('book_series_location_spin').get_value ())
            if series_location == 0:
                # we don't allow books in location 0.
                # instead, we treat them as being unordered in the series
                series_location = None
        else:
            series_location = None

        if physical_book == None:
            #Book
            kwargs = {}
            
            if title: kwargs ['title'] = title
            if subtitle: kwargs ['subtitle'] = subtitle
            if alternate_names: kwargs ['alternate_names'] = alternate_names
            if dewey: kwargs ['dewey'] = dewey
            if copyright_date: kwargs ['copyright_date'] = copyright_date
            if fiction: kwargs ['fiction'] = fiction
            if biography: kwargs ['biography'] = biography
            if series_location != None: kwargs ['series_location'] = series_location
            book = self.library.create_object ('Library.Book', **kwargs)
            
            kwargs = {}
            kwargs ['book'] = book
            if edition: kwargs ['edition'] = edition
            if isbn: kwargs ['isbn'] = isbn
            if printing: kwargs ['printing'] = printing
            if price: kwargs ['price'] = price
            if date_acquired: kwargs ['date_acquired'] = date_acquired
            if number_pages: kwargs ['number_pages'] = number_pages
            if length: kwargs ['length'] = length
            if width: kwargs ['width'] = width
            if height: kwargs ['height'] = height
            if autographed: kwargs ['autographed'] = autographed
            if oversized: kwargs ['oversized'] = oversized
            if publication_city: kwargs ['publication_city'] = publication_city
            if bookstore_topics: kwargs ['bookstore_topics'] = bookstore_topics
            if location: kwargs ['location'] = location
            if type: kwargs ['type'] = type
            physical_book = self.library.create_object ('Library.PhysicalBook', **kwargs)
            physical_book.book = book

        
        if original_language: book.original_language = original_language
        if series: book.series = series

        if language: physical_book.language = language
        if format: physical_book.format = format
        if currency: physical_book.currency = currency
        if publisher: physical_book.publisher = publisher
        if publisher_series: physical_book.publisher_series = publisher_series

        for row in self.subjects_model:
            if row[SubjectsModel.SUBJECT_COLUMN_SELECTED]:
                subject_id = row[SubjectsModel.SUBJECT_COLUMN_ID]
                self.library.db.query ("INSERT into book_subject_link (book_id, subject_id) VALUES (%s, %s)" %
                                       (book._id, subject_id))
        for row in self.physical_person_job_list:
            job_id = row[PersonJobList.PERSON_JOB_COLUMN_JOB_ID]
            person_id = row[PersonJobList.PERSON_JOB_COLUMN_PERSON_ID]
            #FIXME: Replace this once we explicitly
            self.library.db.query ("INSERT into physicalbook_person_link (physical_book_id, person_id, jobs_id) VALUES (%s, %s, %s)" %
                                   (physical_book._id, person_id, job_id))

        for row in self.person_job_list:
            job_id = row[PersonJobList.PERSON_JOB_COLUMN_JOB_ID]
            person_id = row[PersonJobList.PERSON_JOB_COLUMN_PERSON_ID]
            primary = row[PersonJobList.PERSON_JOB_COLUMN_PRIMARY]
            #FIXME: Replace this once we explicitly
            self.library.db.query ("INSERT into book_person_link (book_id, person_id, jobs_id, first) VALUES (%s, %s, %s, %s)" %
                                   (book._id, person_id, job_id, util.quote_value (util.BoolField, primary)))

            

        return physical_book

if __name__ == '__main__':
    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()
    
    gnome.init("Bookworm", "0.1")
    dialog = NewBookDialog (library)
    physical_book = dialog.run ()
