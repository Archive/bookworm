import gtk
import gtk.glade
import Library
import util
from rhpl.GenericError import *
from TestError import *

from OptionData import SuffixWidget, HonorificWidget, NationalityWidget, PersonalTitleWidget
from CityDialog import CityWidget
def _(x): return x

class PersonDialog:
    def __init__ (self, library):
        self.library = library
        self.xml = gtk.glade.XML ('PersonDialog.glade')
        dialog_xml = gtk.glade.XML ('OptionData.glade')

        self.personal_title = PersonalTitleWidget (self.library,
                                                   self.xml.get_widget ('person_name_personal_title_omenu'),
                                                   self.xml.get_widget ('person_name_personal_title_button'),
                                                   xml=dialog_xml)
        self.honorific = HonorificWidget (self.library,
                                          self.xml.get_widget ('person_name_honorific_omenu'),
                                          self.xml.get_widget ('person_name_honorific_button'),
                                          xml=dialog_xml)
        self.suffix = SuffixWidget (self.library,
                                    self.xml.get_widget ('person_name_suffix_omenu'),
                                    self.xml.get_widget ('person_name_suffix_button'),
                                    xml=dialog_xml)
        self.nationality = NationalityWidget (self.library,
                                              self.xml.get_widget ('person_details_nationality_omenu'),
                                              self.xml.get_widget ('person_details_nationality_button'),
                                              xml=dialog_xml)
        self.birth_city = CityWidget (self.library,
                                      self.xml.get_widget ('person_details_birth_city_label'),
                                      self.xml.get_widget ('person_details_birth_city_change_button'),
                                      None,
                                      self.xml.get_widget ('person_details_dialog'))
        self.residence = CityWidget (self.library,
                                          self.xml.get_widget ('person_details_residence_label'),
                                          self.xml.get_widget ('person_details_residence_change_button'),
                                          None,
                                          self.xml.get_widget ('person_details_dialog'))
        self.xml.get_widget ('person_dialog_death_known_toggle').connect ('toggled', self.toggle_guard,
                                                                   self.xml.get_widget ('person_dialog_death_table'))
        self.xml.get_widget ('person_details_birth_year_entry').connect ('insert-text', util.year_entry_insert_text_cb)
        self.xml.get_widget ('person_details_birth_date_entry').connect ('insert-text', util.date_entry_insert_text_cb)
        self.xml.get_widget ('person_details_death_year_entry').connect ('insert-text', util.year_entry_insert_text_cb)
        self.xml.get_widget ('person_details_death_date_entry').connect ('insert-text', util.date_entry_insert_text_cb)

    def setup (self):
        self.xml.get_widget ('person_name_first_name_entry').set_text ('')
        self.xml.get_widget ('person_name_middle_name_entry').set_text ('')
        self.xml.get_widget ('person_name_last_name_entry').set_text ('')
        self.xml.get_widget ('person_details_occupation_entry').set_text ('')
        self.xml.get_widget ('person_details_birth_year_entry').set_text ('')
        self.xml.get_widget ('person_details_birth_date_entry').set_text ('')
        self.xml.get_widget ('person_details_death_year_entry').set_text ('')
        self.xml.get_widget ('person_details_death_date_entry').set_text ('')
        self.xml.get_widget ('person_notes_textview').get_buffer().set_text ('')

        self.personal_title.set_object (None)
        self.honorific.set_object (None)
        self.suffix.set_object (None)
        self.nationality.set_object (None)
        self.birth_city.set_object (None)
        self.residence.set_object (None)
        self.xml.get_widget ('person_dialog_death_table').set_sensitive (False)

    def hydrate (self, person):
        self.setup ()
        self.personal_title.set_object (person.personal_title)
        self.honorific.set_object (person.honorific)
        self.suffix.set_object (person.suffix)
        self.nationality.set_object (person.nationality)
        self.birth_city.set_object (person.birth_city)
        self.residence.set_object (person.residence)
        if person.first_name:
            self.xml.get_widget ('person_name_first_name_entry').set_text (person.first_name)
        if person.middle_name:
            self.xml.get_widget ('person_name_middle_name_entry').set_text (person.middle_name)
        if person.last_name:
            self.xml.get_widget ('person_name_last_name_entry').set_text (person.last_name)
        if person.birth_year:
            self.xml.get_widget ('person_details_birth_year_entry').set_text (str(person.birth_year))
        if person.occupation:
            self.xml.get_widget ('person_details_occupation_entry').set_text (person.occupation)
        if person.death_year:
            self.xml.get_widget ('person_details_death_year_entry').set_text (str(person.death_year))
        if person.notes:
            self.xml.get_widget ('person_notes_textview').get_buffer().set_text (person.notes)
        if person.birth_date:
            self.xml.get_widget ('person_details_birth_date_entry').set_text (person.birth_date)
        self.xml.get_widget ('person_dialog_death_known_toggle').set_active (True)
        if person.death_date == None and person.death_year == None:
            self.xml.get_widget ('person_dialog_death_known_toggle').set_active (False)
            self.xml.get_widget ('person_details_death_date_entry').set_text ('')
            self.xml.get_widget ('person_details_death_year_entry').set_text ('')
        else:
            if person.death_date != None: self.xml.get_widget ('person_details_death_date_entry').set_text (person.death_date)
            if person.death_year != None: self.xml.get_widget ('person_details_death_year_entry').set_text (str(person.death_year))


    def test (self):
        if not (self.xml.get_widget ('person_name_first_name_entry').get_text () or
                self.xml.get_widget ('person_name_middle_name_entry').get_text () or
                self.xml.get_widget ('person_name_last_name_entry').get_text ()):
            raise TestError ("The person must have a name")

    def dehydrate (self, person):
        nationality = self.nationality.get_object ()
        suffix = self.suffix.get_object ()
        personal_title = self.personal_title.get_object ()
        honorific = self.honorific.get_object ()
        residence = self.residence.get_object ()
        birth_city = self.birth_city.get_object ()
        first_name = self.xml.get_widget ('person_name_first_name_entry').get_text ()
        middle_name = self.xml.get_widget ('person_name_middle_name_entry').get_text ()
        last_name = self.xml.get_widget ('person_name_last_name_entry').get_text ()
        birth_year = self.xml.get_widget ('person_details_birth_year_entry').get_text ()
        birth_date = self.xml.get_widget ('person_details_birth_date_entry').get_text ()
        occupation = self.xml.get_widget ('person_details_occupation_entry').get_text ()

        if self.xml.get_widget ('person_dialog_death_known_toggle').get_active ():
            death_year = self.xml.get_widget ('person_details_death_year_entry').get_text ()
            death_date = self.xml.get_widget ('person_details_death_date_entry').get_text ()
        else:
            death_year = None
            death_date = None
        buffer = self.xml.get_widget ('person_notes_textview').get_buffer()
        notes = buffer.get_text (buffer.get_start_iter(), buffer.get_end_iter())

        if person:
            if first_name: person.first_name = first_name
            else: person.first_name = None
            if middle_name: person.middle_name = middle_name
            else: person.middle_name = None
            if last_name: person.last_name = last_name
            else: person.last_name = None
            if birth_year: person.birth_year = int (birth_year)
            else: person.birth_year = None
            if birth_date: person.birth_date = birth_date
            else: person.birth_date = None
            if occupation: person.occupation = occupation
            else: person.occupation = None
            if death_year: person.death_year = int (death_year)
            else: person.death_year = None
            if death_date: person.death_date = death_date
            else: person.death_date = None
            if notes: person.notes = notes
            else: person.notes = None
            if nationality != person.nationality:
                person.nationality = nationality
            if suffix != person.suffix:
                person.suffix = suffix
            if personal_title != person.personal_title:
                person.personal_title = personal_title
            if honorific != person.honorific:
                person.honorific = honorific
            if residence != person.residence:
                person.residence = residence
            if birth_city != person.birth_city:
                person.birth_city = birth_city
        else:
            kwargs = {}
            if first_name: kwargs['first_name'] = first_name
            if middle_name: kwargs['middle_name'] = middle_name
            if last_name: kwargs['last_name'] = last_name
            if birth_year: kwargs['birth_year'] = int (birth_year)
            if birth_date: kwargs['birth_date'] = birth_date
            if occupation: kwargs['occupation'] = occupation
            if death_year: kwargs['death_year'] = int (death_year)
            if death_date: kwargs['death_date'] = death_date
            if notes: kwargs['notes'] = notes
            person = self.library.create_object ('Library.Person', **kwargs)
            if nationality: person.nationality = nationality
            if suffix: person.suffix = suffix
            if personal_title: person.personal_title = personal_title
            if honorific: person.honorific = honorific
            if residence: person.residence = residence
            if birth_city: person.birth_city = birth_city

        return person

    def run (self, person=None):
        dialog = self.xml.get_widget ('person_dialog')

        if person == None:
            self.setup ()
            dialog.set_title (_("Enter a new person"))
        else:
            self.hydrate (person)
            dialog.set_title (_("Edit existing person"))
        while 1:
            button = dialog.run ()
            if button == gtk.RESPONSE_OK:
                try:
                    self.test ()
                except TestError, msg:
                    print msg
                    continue
                person = self.dehydrate (person)
                dialog.destroy ()
                return person
            else:
                dialog.destroy ()
                return None

    def toggle_guard (self, toggle, widget):
        if toggle.get_active():
            widget.set_sensitive (True)
        else:
            widget.set_sensitive (False)

if __name__ == '__main__':
    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()
    xml = gtk.glade.XML ('PersonDialog.glade')
    if len (sys.argv) == 2:
        id = sys.argv[1]
        person = library.get_object ("Library.Person", id)
    else:
        person = None
        id = None
    dialog = PersonDialog (library)
    person = dialog.run (person)
    if person and not id:
        print person._id
