import gtk
import gobject
from rhpl.GenericError import *


class JobsModel(gtk.ListStore):
    JOBS_COLUMN_ID = 0
    JOBS_COLUMN_NAME = 1

    def __init__ (self, library):
        self.library = library
        gtk.ListStore.__init__ (self,
                                gobject.TYPE_INT,    # ID
                                gobject.TYPE_STRING) # NAME
        result = self.library.db.query ("SELECT jobs_id FROM jobs").getresult()
        for (id,) in result:
            job = self.library.get_object ('Library.Jobs', id)
            iter = self.append ()
            self.set (iter,
                      self.JOBS_COLUMN_ID, id,
                      self.JOBS_COLUMN_NAME, job.name)
        self.set_sort_column_id (self.JOBS_COLUMN_NAME, gtk.SORT_ASCENDING)

    def add_job (self, name):
        query = "SELECT jobs_id FROM jobs WHERE name=\'%s\'" % name
        result = self.library.db.query (query).getresult()
        if len (result) > 0:
            raise 'DuplicateError'
        kwargs = {'name': name }
        jobs = self.library.create_object ('Library.Jobs', **kwargs)
        iter = self.append()
        self.set (iter,
                  self.JOBS_COLUMN_ID, jobs.jobs_id,
                  self.JOBS_COLUMN_NAME, jobs.name)
        path = self.get_path (iter)
        return path


class JobsAddEntry:
    def __init__ (self, entry, button, tree_view):
        self.entry = entry
        self.button = button
        self.tree_view = tree_view
        self.model = self.tree_view.get_model ()

        self.entry.connect ('changed', self.entry_changed)
        self.button.connect ('clicked', self.add_job_from_entry)
        self.entry.connect ('activate', self.add_job_from_entry)
        self.entry_changed (self.entry)

    def entry_changed (self, entry):
        if self.entry.get_text() == '':
            self.button.set_sensitive (False)
        else:
            self.button.set_sensitive (True)

    def add_job_from_entry (self, widget):
        new_job = self.entry.get_text()
        if new_job == '':
            return
        
        try:
            path = self.model.add_job (new_job)
        except 'DuplicateError':
            GenericError (TYPE_ERROR,
                          "Job already exists",
                          "A job named \"%s\" already exists in this database." % new_job,
                          parent_dialog=self.entry.get_toplevel(),
                          broken_widget=self.entry).display ()
        else:
            self.entry.set_text ('')
            self.tree_view.set_cursor (path, None, False)
