import gtk
import gtk.glade

from Series import SeriesModel
from TestError import *
import Library

class SeriesWidget:
    def __init__ (self, library, label, location, button, series=None, parent=None):
        assert (library != None)
        assert (label != None)
        assert (button != None)
        self.library = library
        self.label = label
        self.button = button
        self.location = location
        self.parent = parent
        self.button.connect ('clicked', self.select_series)
        self.set_object (series)


    def select_series (self, button):
        dialog = SelectSeriesDialog (self.library, self.series, self.parent)
        series = dialog.run (self.series)
        self.set_object (series)

    def set_object (self, series):
        self.series = series
        if series:
            self.label.set_text (series.name)
            self.location.set_range (0, series.length)
            self.location.set_sensitive (True)
        else:
            self.label.set_text ('No series selected')
            self.location.set_sensitive (False)

    def get_object (self):
        return self.series

class SelectSeriesDialog:
    def __init__ (self, library, series, parent):
        self.library = library
        self.xml = gtk.glade.XML ('SeriesDialog.glade', 'series_select_dialog')
        self.xml.get_widget ('series_select_new_button').connect ('clicked', self.new_series)
        self.model = SeriesModel (self.library)
        tree_view = self.xml.get_widget ('series_select_tree_view')
        tree_view.set_model (self.model)
        tree_view.set_search_column (SeriesModel.SERIES_COLUMN_NAME)
        column = gtk.TreeViewColumn ('Series', gtk.CellRendererText(), text=SeriesModel.SERIES_COLUMN_NAME)
        tree_view.append_column (column)
        column = gtk.TreeViewColumn ('Length', gtk.CellRendererText(), text=SeriesModel.SERIES_COLUMN_LENGTH)
        tree_view.append_column (column)
        tree_view.get_selection().connect ('changed', self.selection_changed)
        if parent:
            self.xml.get_widget ('series_select_dialog').set_transient_for (parent)

    def selection_changed (self, selection):
        (unused, row) = selection.get_selected ()

        if row:
            self.xml.get_widget ('ok_button').set_sensitive (True)
        else:
            self.xml.get_widget ('ok_button').set_sensitive (False)

    def run (self, series):
        dialog = self.xml.get_widget ('series_select_dialog')
        button = dialog.run ()
        if button == gtk.RESPONSE_OK:
            selection = self.xml.get_widget ('series_select_tree_view').get_selection ()
            (model, row) = selection.get_selected ()
            id = model[row][0]
            if id > 0:
                series = self.library.get_object ('Library.Series', id)
            else:
                series = None
        dialog.destroy ()
        return series

    def new_series (self, button):
        dialog = SeriesDialog (self.library, self.xml.get_widget ('series_select_dialog'))
        series = dialog.run ()
        if series:
            path = self.model.add_series (series)
            selection = self.xml.get_widget ('series_select_tree_view').get_selection ()
            selection.select_path (path)

class SeriesDialog:
    def __init__ (self, library, parent=None):
        self.library = library
        self.xml = gtk.glade.XML ('SeriesDialog.glade', 'series_dialog')
        if parent:
            self.xml.get_widget ('series_dialog').set_transient_for (parent)

    def run (self, series=None):
        dialog = self.xml.get_widget ('series_dialog')
        if series == None:
            self.setup ()
            dialog.set_title ("Create a new series")
        else:
            self.hydrate (series)
            dialog.set_title ("Edit existing series")

        while 1:
            button = dialog.run ()
            if button == gtk.RESPONSE_OK:
                try:
                    self.test ()
                except TestError, msg:
                    print msg
                    continue
                series = self.dehydrate (series)
                dialog.destroy ()
                return series
            else:
                dialog.destroy ()
                return None

    def test (self):
        if not self.xml.get_widget ('series_name_entry').get_text ():
            raise TestError ("Series must have a name")

    def setup (self):
        self.xml.get_widget ('series_name_entry').set_text ('')
        self.xml.get_widget ('series_length_spin').set_value (0)
        self.xml.get_widget ('series_notes_text_view').get_buffer ().set_text ('')
        
    def hydrate (self, series):
        self.xml.get_widget ('series_name_entry').set_text (series.name)
        if series.length: self.xml.get_widget ('series_length_spin').set_value(series.length)
        if series.notes: self.xml.get_widget ('series_notes_text_view').get_buffer ().set_text (series.notes)

    def dehydrate (self, series):
        name = self.xml.get_widget ('series_name_entry').get_text ()
        length = int (self.xml.get_widget ('series_length_spin').get_value ())
        buffer = self.xml.get_widget ('series_notes_text_view').get_buffer ()
        notes = buffer.get_text (buffer.get_start_iter(), buffer.get_end_iter())

        if series:
            series.name = name
            series.length = length
            series.notes = notes
        else:
            kwargs = {}
            kwargs['name'] = name
            kwargs['length'] = length
            if notes: kwargs['notes'] = notes
            series = self.library.create_object ('Library.Series', **kwargs)
        return series

if __name__ == '__main__':
    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()
    if len (sys.argv) == 2:
        id = sys.argv[1]
        series = library.get_object ("Library.Series", id)
    else:
        id = None
        series = None
    dialog = SeriesDialog (library)
    series = dialog.run (series)
    if series:
        print series._id
    
