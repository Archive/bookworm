import gtk
import gtk.glade
import Library
import util
import gnome.ui
import time

from OptionData import LanguageWidget
from CityDialog import CityWidget
from Subjects import SubjectsModel, SubjectsAddEntry
from Jobs import JobsModel, JobsAddEntry

def _(x): return x

## Each page has an init function called which sets its up.  No actual
## objects are made until the final finish call is made.  We need to
## thus carefully check the data to make sure that there aren't any
## problems here.  Each page stores it's information in the BookInfo
## class.  There, a set of kword args are kept.  When everything is
## done, then all the classes in the database are created.
## 
## Exceptions to this rule are all the various attributes, such as
## Jobs, Titles, etc.


class BookInfo:
    def __init__ (self):
        self.book_kwargs = {}
        self.book_subjects = []
        self.job_relationship_list = []

    def commit_to_library (self, library):
        try:
            library.create_object ('Library.Book', **self.book_kwargs)
        except:
            return

class NewBookDruid:
    PHYSICAL_BOOK=0
    AUDIO_BOOK=1
    E_BOOK=2
    def __init__ (self, library):
        self.xml = gtk.glade.XML ('NewBookDruid.glade')
        self.library = library

        # Set all internal variables
        self.book_type = self.PHYSICAL_BOOK
        self.existing_book = False
        self.book_info = BookInfo ()

        ## Initialize each page
        self.book_type_druid_page_init ()
        self.create_book_druid_page_init ()
        self.book_subjects_druid_page_init ()
        self.book_jobs_druid_page_init ()
        self.book_person_druid_page_init ()
        self.select_book_druid_page_init ()
        self.physical_book_druid_page_init ()
        self.physical_book_info_druid_page_init ()
        self.audio_book_druid_page_init ()
        self.e_book_druid_page_init ()
        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_subjects_druid_page'))

    def run (self):
        self.xml.get_widget ('new_book_window').show_all ()
        gtk.mainloop ()


    ## book_type_druid_page
    def book_type_druid_page_init (self):
        page = self.xml.get_widget ('book_type_druid_page')
        page.connect_after ('prepare', self.book_type_druid_page_prepare)
        page.connect ('next', self.book_type_druid_page_next)
        
    def book_type_druid_page_prepare (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_buttons_sensitive (False, True, True, True)
        if self.book_type == self.PHYSICAL_BOOK:
            self.xml.get_widget ('book_type_physical_book_radio').set_active (True)
        elif self.book_type == self.AUDIO_BOOK:
            self.xml.get_widget ('book_type_audio_book_radio').set_active (True)
        elif self.book_type == self.E_BOOK:
            self.xml.get_widget ('book_type_e_book_radio').set_active (True)
        self.xml.get_widget ('book_type_existing_book_toggle').set_active (self.existing_book)
        return True

    def book_type_druid_page_next (self, druid_page, druid):
        self.existing_book = self.xml.get_widget ('book_type_existing_book_toggle').get_active ()
        if self.xml.get_widget ('book_type_physical_book_radio').get_active ():
            self.book_type = self.PHYSICAL_BOOK
        elif self.xml.get_widget ('book_type_audio_book_radio').get_active ():
            self.book_type = self.AUDIO_BOOK
        elif self.xml.get_widget ('book_type_e_book_radio').get_active ():
            self.book_type = self.E_BOOK

        if self.existing_book:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('select_book_druid_page'))
        else:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('create_book_druid_page'))
        return True

    ## create_book_druid_page
    def create_book_druid_page_init (self):
        page = self.xml.get_widget ('create_book_druid_page')
        page.connect ('prepare', self.create_book_druid_page_prepare)
        page.connect ('back', self.create_book_druid_page_back)
        page.connect ('next', self.create_book_druid_page_next)
        omenu = self.xml.get_widget ('create_book_original_language_omenu')
        self.lang_widget = LanguageWidget (self.xml, library, omenu)

    def create_book_druid_page_prepare (self, druid_page, druid):
        return True


    def create_book_druid_page_test (self):
        if self.xml.get_widget ('create_book_title_entry').get_text() == "":
            raise 'Error'
        

    def create_book_druid_page_next (self, druid_page, druid):
        try:
            self.create_book_druid_page_test ()
        except:
            return True
        kwargs = {}
        text = self.xml.get_widget ('create_book_title_entry').get_text()
        kwargs['title'] = text

        text = self.xml.get_widget ('create_book_subtitle_entry').get_text()
        if text: kwargs['subtitle'] = text

        text = self.xml.get_widget ('create_book_alternate_names_entry').get_text()
        if text: kwargs['alternate_names'] = text

        text = self.xml.get_widget ('create_book_dewey_entry').get_text()
        if text: kwargs['dewey'] = text

        date = self.xml.get_widget ('create_book_copyright_date_entry').get_text ()
        if date: kwargs['copyright_date'] = date

        kwargs['fiction'] = self.xml.get_widget ('create_book_fiction_toggle').get_active ()

        kwargs['biography'] = self.xml.get_widget ('create_book_biography_toggle').get_active ()

        buffer = self.xml.get_widget ('create_book_notes_text_view').get_buffer ()
        text = buffer.get_text (buffer.get_start_iter(), buffer.get_end_iter())
        if text: kwargs['notes'] = text

        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_subjects_druid_page'))

        # Store the data for later
        self.book_info.book_kwargs = kwargs

        return True

    def create_book_druid_page_back (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_type_druid_page'))
        self.book_info.book_kwargs = {}
        return True

    ## book_subjects_druid_page
    def book_subjects_druid_page_init (self):
        page = self.xml.get_widget ('book_subjects_druid_page')
        page.connect ('prepare', self.book_subjects_druid_page_prepare)
        page.connect ('back', self.book_subjects_druid_page_back)
        page.connect ('next', self.book_subjects_druid_page_next)

        model = SubjectsModel(self.library)
        tree_view = self.xml.get_widget ('book_subjects_tree_view')
        tree_view.set_search_column (SubjectsModel.SUBJECT_COLUMN_NAME)
        tree_view.set_model (model)
        cell = gtk.CellRendererToggle()
        def cell_toggled (cell, path_str, model):
            iter = model.get_iter (path_str)
            toggled = model.get_value (iter, SubjectsModel.SUBJECT_COLUMN_SELECTED)
            toggled = not toggled
            model.set (iter, 1, toggled)
        cell.connect ('toggled', cell_toggled, model)
        column = gtk.TreeViewColumn (None, cell, active=SubjectsModel.SUBJECT_COLUMN_SELECTED)
        tree_view.append_column (column)
        cell = gtk.CellRendererText()
        column = gtk.TreeViewColumn ("Subjects", cell, text=SubjectsModel.SUBJECT_COLUMN_NAME)
        tree_view.append_column (column)
        self.subjects_add_entry = SubjectsAddEntry (self.xml.get_widget ('book_subjects_new_subject_entry'),
                                                    self.xml.get_widget ('book_subject_new_subject_button'),
                                                    tree_view)


    def book_subjects_druid_page_prepare (self, druid_page, druid):
        tree_view = self.xml.get_widget ('book_subjects_tree_view')
        tree_view.grab_focus()
        tree_view.set_cursor ((0, ), None, False)

        return True

    def book_subjects_druid_page_next (self, druid_page, druid):
        tree_view = self.xml.get_widget ('book_subjects_tree_view')
        model = tree_view.get_model ()
        subject_ids = []
        for iter in model:
            if iter[SubjectsModel.SUBJECT_COLUMN_SELECTED]:
                subject_ids.append (iter[SubjectsModel.SUBJECT_COLUMN_ID])
        self.book_info.book_subjects = subject_ids
        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_jobs_druid_page'))
        return True

    def book_subjects_druid_page_back (self, druid_page, druid):
        self.book_info.book_subjects = []
        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('create_book_druid_page'))
        return True

    ## book_jobs_druid_page
    def book_jobs_druid_page_init (self):
        page = self.xml.get_widget ('book_jobs_druid_page')
        page.connect ('prepare', self.book_jobs_druid_page_prepare)
        page.connect ('back', self.book_jobs_druid_page_back)
        page.connect ('next', self.book_jobs_druid_page_next)

        def radio_toggled (toggle, self):
            if self.xml.get_widget ('book_jobs_create_person_radio').get_active ():
                self.xml.get_widget ('book_jobs_swindow').set_sensitive (True)
            else:
                self.xml.get_widget ('book_jobs_swindow').set_sensitive (False)
        self.xml.get_widget ('book_jobs_create_person_radio').connect ('toggled', radio_toggled, self)

        model = JobsModel(self.library)
        tree_view = self.xml.get_widget ('book_jobs_tree_view')
        tree_view.set_search_column (JobsModel.JOBS_COLUMN_NAME)
        tree_view.set_model (model)
        cell = gtk.CellRendererText()
        column = gtk.TreeViewColumn ("Jobs", cell, text=JobsModel.JOBS_COLUMN_NAME)
        tree_view.append_column (column)
        self.jobs_add_entry = JobsAddEntry (self.xml.get_widget ('book_jobs_new_subject_entry'),
                                            self.xml.get_widget ('book_jobs_new_subject_button'),
                                            tree_view)

    def book_jobs_druid_page_prepare (self, druid_page, druid):
        self.xml.get_widget ('book_jobs_create_person_radio').set_active (True)
        tree_view = self.xml.get_widget ('book_jobs_tree_view')
        tree_view.grab_focus ()
        tree_view.set_cursor ((0, ), None, False)

    def book_jobs_druid_page_back (self, druid_page, druid):
        self.book_info.job_relationship_list = self.book_info.job_relationship_list[:-1]

        if self.book_info.job_relationship_list == []:
            self.xml.get_widget ('new_book_druid').set_page ('book_subjects_druid_page')
        else:
            self.xml.get_widget ('new_book_druid').set_page ('book_person_druid_page')
        return True

    def book_jobs_druid_page_next (self, druid_page, druid):
        if self.xml.get_widget ('book_jobs_create_person_radio').get_active ():
            # we want to make a new person/job listing.  Find out what the job is
            selection = self.xml.get_widget ('book_jobs_tree_view').get_selection()
            (model, iter) = selection.get_selected ()
            row
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_person_druid_page'))
        elif self.book_type == self.PHYSICAL_BOOK:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('physical_book_druid_page'))
        elif self.book_type == self.AUDIO_BOOK:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('audio_book_druid_page'))
        elif self.book_type == self.E_BOOK:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('e_book_druid_page'))
        else:
            raise 'AssertNotReached'
        return True

    ## book_person_druid_page
    def book_person_druid_page_init (self):
        pass
    
    
    def book_info_druid_page_prepare (self, druid_page, druid):
        return True

    def book_info_druid_page_next (self, druid_page, druid):
        if 0:
            pass
        else:
            if self.book_type == self.PHYSICAL_BOOK:
                self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('physical_book_druid_page'))
            elif self.book_type == self.AUDIO_BOOK:
                self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('audio_book_druid_page'))
            elif self.book_type == self.E_BOOK:
                self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('e_book_druid_page'))
        return True

    def book_info_druid_page_back (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('create_book_druid_page'))
        return True

    def book_subjects_entry_changed (self, entry, *args):
        if entry.get_text () == "":
            self.xml.get_widget ('book_subject_new_subject_button').set_sensitive (False)
        else:
            self.xml.get_widget ('book_subject_new_subject_button').set_sensitive (True)

    ## select_book_druid_page
    def select_book_druid_page_init (self):
        page = self.xml.get_widget ('select_book_druid_page')
        page.connect ('prepare', self.select_book_druid_page_prepare)
        page.connect ('back', self.select_book_druid_page_back)
        page.connect ('next', self.select_book_druid_page_next)

    def select_book_druid_page_prepare (self, druid_page, druid):
        return True

    def select_book_druid_page_next (self, druid_page, druid):
        if self.book_type == self.PHYSICAL_BOOK:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('physical_book_druid_page'))
        elif self.book_type == self.AUDIO_BOOK:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('audio_book_druid_page'))
        elif self.book_type == self.E_BOOK:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('e_book_druid_page'))
        return True

    def select_book_druid_page_back (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_type_druid_page'))
        return True


    ## physical_book_druid_page
    def physical_book_druid_page_init (self):
        page = self.xml.get_widget ('physical_book_druid_page')
        page.connect ('prepare', self.physical_book_druid_page_prepare)
        page.connect ('back', self.physical_book_druid_page_back)
        page.connect ('next', self.physical_book_druid_page_next)

    def physical_book_druid_page_prepare (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_show_finish (False)
        return True

    def physical_book_druid_page_next (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('physical_book_info_druid_page'))
        return True

    def physical_book_druid_page_back (self, druid_page, druid):
        if self.existing_book:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('select_book_druid_page'))
        else:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_info_druid_page'))
        return True

    ## physical_book_info_druid_page
    def physical_book_info_druid_page_init (self):
        page = self.xml.get_widget ('physical_book_info_druid_page')
        page.connect_after ('prepare', self.physical_book_info_druid_page_prepare)
        page.connect ('back', self.physical_book_info_druid_page_back)
        page.connect ('finish', self.physical_book_info_druid_page_finish)

    def physical_book_info_druid_page_prepare (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_show_finish (True)
        return True

    def physical_book_info_druid_page_finish (self, druid_page, druid):
        gtk.mainquit ()

    def physical_book_info_druid_page_back (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_show_finish (False)
        self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('physical_book_druid_page'))
        return True


    ## audio_book_druid_page
    def audio_book_druid_page_init (self):
        page = self.xml.get_widget ('audio_book_druid_page')
        page.connect_after ('prepare', self.audio_book_druid_page_prepare)
        page.connect ('back', self.audio_book_druid_page_back)
        page.connect ('finish', self.audio_book_druid_page_finish)

    def audio_book_druid_page_prepare (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_show_finish (True)
        print self.xml.get_widget ('new_book_druid').get_property ('show_finish')
        return True

    def audio_book_druid_page_finish (self, druid_page, druid):
        gtk.mainquit ()

    def audio_book_druid_page_back (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_show_finish (False)
        if self.existing_book:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('select_book_druid_page'))
        else:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_info_druid_page'))
        return True

    ## e_book_druid_page
    def e_book_druid_page_init (self):
        page = self.xml.get_widget ('e_book_druid_page')
        page.connect_after ('prepare', self.e_book_druid_page_prepare)
        page.connect ('back', self.e_book_druid_page_back)
        page.connect ('finish', self.e_book_druid_page_finish)

    def e_book_druid_page_prepare (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_show_finish (True)
        return True

    def e_book_druid_page_finish (self, druid_page, druid):
        gtk.mainquit ()

    def e_book_druid_page_back (self, druid_page, druid):
        self.xml.get_widget ('new_book_druid').set_show_finish (False)
        if self.existing_book:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('select_book_druid_page'))
        else:
            self.xml.get_widget ('new_book_druid').set_page (self.xml.get_widget ('book_info_druid_page'))
        return True


if __name__ == '__main__':
    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()
    
    gnome.init("Bookworm", "0.1")
    druid = NewBookDruid (library)
    druid.run ()
