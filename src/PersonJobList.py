import gtk
import gtk.glade
import gobject
from PersonDialog import PersonDialog
from Jobs import JobsModel, JobsAddEntry
from Person import PersonModel
from TestError import TestError
import Library
from gettext import gettext as _


class PersonJobDialog:
    def __init__ (self, library):
        self.library = library
        self.xml = gtk.glade.XML ('PersonJobList.glade')
        self.initialize ()

    def initialize (self):
        self.jobs = JobsModel (self.library)
        tree_view = self.xml.get_widget ('job_tree_view')
        tree_view.set_model (self.jobs)
        column = gtk.TreeViewColumn (_("Job"), gtk.CellRendererText (),
                                     text=JobsModel.JOBS_COLUMN_NAME)
        tree_view.append_column (column)
        tree_view.get_selection().connect ('changed', self.selection_changed)
        self.jobs_add_entry = JobsAddEntry (self.xml.get_widget ('new_job_entry'),
                                            self.xml.get_widget ('new_job_button'),
                                            self.xml.get_widget ('job_tree_view'))
        tree_view.set_search_column (JobsModel.JOBS_COLUMN_NAME)

        self.persons = PersonModel (self.library)
        tree_view = self.xml.get_widget ('person_tree_view')
        tree_view.set_model (self.persons)
        column = gtk.TreeViewColumn (_("Person"), gtk.CellRendererText (),
                                     text=PersonModel.PERSON_COLUMN_NAME)
        tree_view.append_column (column)
        tree_view.get_selection().connect ('changed', self.selection_changed)
        self.xml.get_widget ('new_person_button').connect ('clicked', self.new_person)

        tree_view.set_search_column (PersonModel.PERSON_COLUMN_NAME)
        self.selection_changed (None)

    def selection_changed (self, selection):
        selection1 = self.xml.get_widget ('job_tree_view').get_selection ()
        selection2 = self.xml.get_widget ('person_tree_view').get_selection ()

        (unused, row1) = selection1.get_selected ()
        (unused, row2) = selection2.get_selected ()

        if row1 and row2:
            self.xml.get_widget ('ok_button').set_sensitive (True)
        else:
            self.xml.get_widget ('ok_button').set_sensitive (False)

    def setup (self):
        pass

    def dehydrate (self):
        pass

    def run (self):
        dialog = self.xml.get_widget ('person_job_dialog')

        self.setup ()
        dialog.set_title (_("Create a new person/job"))
        while 1:
            button = dialog.run ()
            if button == gtk.RESPONSE_OK:
                selection = self.xml.get_widget ('person_tree_view').get_selection()
                (model, iter) = selection.get_selected ()
                id = model.get_value (iter, PersonModel.PERSON_COLUMN_ID)
                person = self.library.get_object ('Library.Person', id)
                
                selection = self.xml.get_widget ('job_tree_view').get_selection()
                (model, iter) = selection.get_selected ()
                id = model.get_value (iter, JobsModel.JOBS_COLUMN_ID)
                job = self.library.get_object ('Library.Jobs', id)
                dialog.hide ()
                return (person, job)
            else:
                dialog.hide ()
                return (None, None)
    
    def new_person (self, button):
        dialog = PersonDialog (self.library)
        person = dialog.run ()
        if person != None:
            path = self.persons.add_person (person)
            selection = self.xml.get_widget ('person_tree_view').get_selection()
            selection.select_path (path)

class PersonJobList (gtk.ListStore):
    PERSON_JOB_COLUMN_JOB_DESCRIPTION = 0
    PERSON_JOB_COLUMN_PERSON_NAME = 1
    PERSON_JOB_COLUMN_JOB_ID = 2
    PERSON_JOB_COLUMN_PERSON_ID = 3
    PERSON_JOB_COLUMN_PRIMARY = 4

    def __init__ (self, library):
        gtk.ListStore.__init__ (self,
                                gobject.TYPE_STRING,   # JOB_COLUMN
                                gobject.TYPE_STRING,   # PERSON_NAME
                                gobject.TYPE_INT,      # JOB_ID
                                gobject.TYPE_INT,      # PERSON_ID
                                gobject.TYPE_BOOLEAN)  # PRIMARY
        self.library = library
        self.set_sort_column_id (self.PERSON_JOB_COLUMN_JOB_DESCRIPTION, gtk.SORT_ASCENDING)
        self.person_job_dialog = None

    def add_buttons (self, tree_view, add_button, edit_button, delete_button):
        self.tree_view = tree_view
        tree_view.get_selection().connect ('changed', self.tree_selection_changed, edit_button, delete_button)
        add_button.connect ('clicked', self.add_person_job)
        edit_button.connect ('clicked', self.edit_person_job)
        delete_button.connect ('clicked', self.delete_person_job)
        edit_button.set_sensitive (False)
        delete_button.set_sensitive (False)

    def tree_selection_changed (self, selection, edit_button, delete_button):

        (model, iter) = selection.get_selected ()
        if iter == None:
            edit_button.set_sensitive (False)
            delete_button.set_sensitive (False)
        else:
            edit_button.set_sensitive (True)
            delete_button.set_sensitive (True)


    def add_person_job (self, button):
        if self.person_job_dialog == None:
            self.person_job_dialog = PersonJobDialog (self.library)
        (person, job) = self.person_job_dialog.run ()
        if person:
            iter = self.append ()
            self.set (iter,
                      self.PERSON_JOB_COLUMN_JOB_DESCRIPTION, job.name,
                      self.PERSON_JOB_COLUMN_PERSON_NAME, person.get_formal_name (),
                      self.PERSON_JOB_COLUMN_JOB_ID, job._id,
                      self.PERSON_JOB_COLUMN_PERSON_ID, person._id)

    def edit_person_job (self, button):
        print 'fixme: need to write, add and delete instead for now'

    def delete_person_job (self, button):
        (model, row) = self.tree_view.get_selection ().get_selected ()
        self.remove (row)

if __name__ == '__main__':
    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()
    dialog = PersonJobDialog (library)
    dialog.run ()
    
