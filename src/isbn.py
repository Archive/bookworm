#!/usr/bin/python
class isbn:

    def __init__ (self, isbn):
        self.isbn=isbn
        if (not len (self.isbn) == 13) or (not self.isbn[11]=='-'):
            raise 'InvalidISBN'
        self.isbn=isbn.replace('-', '')
        if not len (self.isbn) == 10:
            raise 'InvalidISBN'
        digits = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
        for i in xrange (0, 10):
            if not self.isbn[i] in digits:
                if i == 9 and self.isbn[i] =='X':
                    pass
                else:
                    raise 'InvalidISBN'                        
        self.validate()

    def validate (self):
        x = 0
        for i in xrange (0, 10):
            if self.isbn[i] == 'X':
                x += 10
            else:
                x += int(self.isbn[i]) * (10-i)
        if not x%11 == 0:
            raise 'InvalidISBN'

if __name__ == "__main__":
    try: 
        i = isbn('0-307-24411-3')

    except 'InvalidISBN':
        print 'Ouch'
    else:
        print 'Yea!'

