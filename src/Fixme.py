#!/usr/bin/python
# Fix up a mistake in setting middle names

import gtk
import gtk.glade
import Library
import util
import gnome.ui
import time
import gobject
import xml.sax.saxutils

from gettext import gettext as _
from TestError import *

from PersonDialog import PersonDialog

from NotesWidget import NotesWidget
from PublisherDialog import PublisherModel
from PublisherSeriesWidget import PublisherSeriesWidget

from Subjects import SubjectsModel, SubjectsAddEntry
from PersonJobList import PersonJobList
from SeriesWidget import SeriesWidget



class FixmeModel(gtk.ListStore):
    PERSON_ID = 0
    PERSON_FIRST_NAME = 1
    PERSON_MIDDLE_NAME = 2
    PERSON_LAST_NAME = 3

    PERSON_SORT_COLUMN = 4

    def __init__ (self, library):
        self.library = library
        gtk.ListStore.__init__ (self,
                                gobject.TYPE_INT,    # ID
                                gobject.TYPE_STRING, # FIRST NAME
                                gobject.TYPE_STRING, # MIDDLE NAME
                                gobject.TYPE_STRING) # LAST NAME
        result = self.library.db.query ("SELECT person_id, first_name, last_name FROM person WHERE middle_name = 'FIXME'").getresult()
        i = 0
        for (id, first_name, last_name) in result:
#            person = self.library.get_object ('Library.Person', id)
            iter = self.append ()
            self.set (iter,
                      self.PERSON_ID, id,
                      self.PERSON_FIRST_NAME, first_name,
                      self.PERSON_MIDDLE_NAME, 'FIXME',
                      self.PERSON_LAST_NAME, last_name)
            i += 1
            if (i % 100 == 0):
                print '%d/%d' % (i, len (result))
        self.set_sort_func (self.PERSON_SORT_COLUMN, self.sort_func)
        self.set_sort_column_id (self.PERSON_SORT_COLUMN, gtk.SORT_ASCENDING)

    def sort_func (self, model, a, b):
        first_a = model[a][self.PERSON_FIRST_NAME]
        middle_a = model[a][self.PERSON_MIDDLE_NAME]
        last_a = model[a][self.PERSON_LAST_NAME]
        first_b = model[b][self.PERSON_FIRST_NAME]
        middle_b = model[b][self.PERSON_MIDDLE_NAME]
        last_b = model[b][self.PERSON_LAST_NAME]

        if middle_a != 'FIXME' and middle_b == 'FIXME':
            return -1
        if middle_a == 'FIXME' and middle_b != 'FIXME':
            return 1
        
        if not first_a and first_b:
            return -1
        if first_a and not first_b:
            return 1

        if not last_a and last_b:
            return -1
        if last_a and not last_b:
            return 1

        if last_a < last_b:
            return -1
        elif last_a > last_b:
            return 1
        else:
            if first_a < first_b:
                return -1
            elif first_a > first_b:
                return 1
        return 0

class Fixme:
    def __init__ (self, library):
        self.library = library
        self.xml = gtk.glade.XML ('fixme.glade')
        self.initialize_dialog ()

    def initialize_dialog (self):
        self.clear_button = self.xml.get_widget ('fixme_clear_button')
        self.clear_button.connect ('clicked', self.clear)
        self.fixme_button = self.xml.get_widget ('fixme_fixme_button')
        self.fixme_button.connect ('clicked', self.fixme)

        self.model = FixmeModel (library)
        self.tree_view = self.xml.get_widget ('fixme_tree_view')
        self.tree_view.set_search_column (FixmeModel.PERSON_LAST_NAME)
        self.tree_view.connect ('row-activated', self.row_activated)
        self.tree_view.set_model (self.model)
        self.tree_view.get_selection().connect ('changed', self.selection_changed)
        self.tree_view.get_selection().set_mode (gtk.SELECTION_MULTIPLE)
        column = gtk.TreeViewColumn (_("Last"), gtk.CellRendererText (),
                                     text=FixmeModel.PERSON_LAST_NAME)
        self.tree_view.append_column (column)
        
        column = gtk.TreeViewColumn (_("First"), gtk.CellRendererText (),
                                     text=FixmeModel.PERSON_FIRST_NAME)
        self.tree_view.append_column (column)

        cell = gtk.CellRendererText ()
        cell.set_property ('editable', True)
        cell.connect ('edited', self.row_edited)
        column = gtk.TreeViewColumn (_("Middle"), cell,
                                     text=FixmeModel.PERSON_MIDDLE_NAME)
        self.tree_view.append_column (column)

    def selection_changed (self, selection):
        (model, rows) = selection.get_selected_rows ()
        if rows:
            self.clear_button.set_sensitive (True)
            self.fixme_button.set_sensitive (True)
        else:
            self.clear_button.set_sensitive (False)
            self.fixme_button.set_sensitive (False)

    def row_activated (self, tree_view, path, column):
        id = self.model[path][FixmeModel.PERSON_ID]
        person = self.library.get_object ('Library.Person', id)
        dialog = PersonDialog (library)
        person = dialog.run (person)
        if person:
            self.model[path][FixmeModel.PERSON_FIRST_NAME] = person.first_name
            self.model[path][FixmeModel.PERSON_MIDDLE_NAME] = person.middle_name
            self.model[path][FixmeModel.PERSON_LAST_NAME] = person.last_name

    def row_edited (self, cell, path, text):
        id = self.model[path][FixmeModel.PERSON_ID]
        print id
        person = self.library.get_object ('Library.Person', id)
        person.middle_name = text
        self.model[path][FixmeModel.PERSON_MIDDLE_NAME] = text

    def set_selected_text (self, text):
        selection = self.tree_view.get_selection ()
        
        (model, rows) = selection.get_selected_rows ()
        for row in rows:
            id = model[row][FixmeModel.PERSON_ID]
            person = self.library.get_object ('Library.Person', id)
            person.middle_name = text
            model[row][FixmeModel.PERSON_MIDDLE_NAME] = text

    def clear (self, button):
        self.set_selected_text (None)

    def fixme (self, button):
        self.set_selected_text ('FIXME')

    def run (self):
        dialog = self.xml.get_widget ('fixme_dialog')
        dialog.show_all ()
        dialog.maximize ()
        dialog.run ()


if __name__ == '__main__':
    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()
    
    gnome.init("bookworm-fixme", "0.1")
    dialog = Fixme (library)
    dialog.run ()
