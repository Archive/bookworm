import gtk
import gobject
from rhpl.GenericError import *
import util

class SubjectsModel(gtk.ListStore):
    SUBJECT_COLUMN_ID = 0
    SUBJECT_COLUMN_SELECTED = 1
    SUBJECT_COLUMN_NAME = 2

    def __init__ (self, library):
        self.library = library
        gtk.ListStore.__init__ (self,
                                gobject.TYPE_INT,    # ID
                                gobject.TYPE_INT,    # SELECTED
                                gobject.TYPE_STRING) # NAME
        result = self.library.db.query ("SELECT subject_id FROM subject").getresult()
        for (id,) in result:
            subject = self.library.get_object ('Library.Subject', id)
            iter = self.append ()
            self.set (iter,
                      self.SUBJECT_COLUMN_ID, id,
                      self.SUBJECT_COLUMN_NAME, subject.name)
        self.set_sort_column_id (self.SUBJECT_COLUMN_NAME, gtk.SORT_ASCENDING)

    def add_subject (self, name):
        query = "SELECT subject_id FROM subject WHERE name=\'%s\'" % util.escape_string (name)
        result = self.library.db.query (query).getresult()
        if len (result) > 0:
            raise 'DuplicateError'
        kwargs = {'name': name }
        subject = self.library.create_object ('Library.Subject', **kwargs)
        iter = self.append()
        self.set (iter,
                  self.SUBJECT_COLUMN_ID, subject.subject_id,
                  self.SUBJECT_COLUMN_NAME, subject.name)
        path = self.get_path (iter)
        return path

class SubjectsAddEntry:
    def __init__ (self, entry, button, tree_view):
        self.entry = entry
        self.button = button
        self.tree_view = tree_view
        self.model = self.tree_view.get_model ()

        self.entry.connect ('changed', self.entry_changed)
        self.entry.connect ('activate', self.add_topic)
        self.button.connect ('clicked', self.add_topic)
        self.entry_changed (self.entry)

    def entry_changed (self, entry):
        if self.entry.get_text() == '':
            self.button.set_sensitive (False)
        else:
            self.button.set_sensitive (True)

    def add_topic (self, button):
        new_subject = self.entry.get_text()
        try:
            path = self.model.add_subject (new_subject)
        except 'DuplicateError':
            GenericError (TYPE_ERROR,
                          "Subject already exists",
                          "A subject named \"%s\" already exists in this database." % new_subject,
                          parent_dialog=self.entry.get_toplevel(),
                          broken_widget=self.entry).display ()
        else:
            self.entry.set_text ('')
            self.tree_view.set_cursor (path, None, False)
            self.model[path][SubjectsModel.SUBJECT_COLUMN_SELECTED] = True
