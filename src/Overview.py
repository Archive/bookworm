import gtk
import gtk.glade
import Library
import util
import gnome
import gnome.ui
import gobject
import NewBookDruid

gnome.init("Bookworm", "0.1")

class OverviewList (gtk.TreeView):
    VIEW_BY_BOOKS=0
    VIEW_BY_AUTHORS=1

    BOOK_COLUMN_ID=0
    BOOK_COLUMN_DEWEY=1
    BOOK_COLUMN_TITLE=2
    BOOK_COLUMN_SUBTITLE=3
    BOOK_COLUMN_AUTHOR_LIST=4

    AUTHOR_COLUMN_ID=0
    AUTHOR_COLUMN_NAME=1
    AUTHOR_COLUMN_DATES=2

    def __init__ (self, library):
        gtk.TreeView.__init__ (self)
        self.set_enable_search (True)
        self.library = library
        self.view_type = self.VIEW_BY_AUTHORS
        self.set_view_by_books ()

    def remove_all_columns (self):
        c = self.get_column (0)
        while c:
            self.remove_column (c)
            c = self.get_column (0)


    def set_view_by_authors (self):

        self.remove_all_columns ()
        self.set_search_column (self.AUTHOR_COLUMN_NAME)
        c = gtk.TreeViewColumn ("Name", gtk.CellRendererText (), text=self.AUTHOR_COLUMN_NAME)
        c.set_sort_column_id (self.AUTHOR_COLUMN_NAME)
        self.append_column (c)
        c = gtk.TreeViewColumn ("Dates", gtk.CellRendererText (), text=self.AUTHOR_COLUMN_DATES)
        self.append_column (c)
        c.set_sort_column_id (self.AUTHOR_COLUMN_DATES)
        model = gtk.ListStore (gobject.TYPE_INT,    # ID
                               gobject.TYPE_STRING, # NAME
                               gobject.TYPE_STRING) # DATE

        result = library.db.query ("SELECT person_id FROM person").getresult()
        for (id,) in result:
            person = self.library.get_object ('Library.Person', id)
            iter = model.append ()
            name = person.get_formal_name ()
            
            model.set (iter,
                       self.AUTHOR_COLUMN_ID, id,
                       self.AUTHOR_COLUMN_NAME, name,
                       self.AUTHOR_COLUMN_DATES, '')
        self.set_model (model)

    def set_view_by_books (self):
        self.remove_all_columns ()

        self.set_search_column (self.BOOK_COLUMN_TITLE)
        c = gtk.TreeViewColumn ("Dewey", gtk.CellRendererText (), text=self.BOOK_COLUMN_DEWEY)
        self.append_column (c)
        c.set_sort_column_id (self.BOOK_COLUMN_DEWEY)
        c = gtk.TreeViewColumn ("Title", gtk.CellRendererText (), text=self.BOOK_COLUMN_TITLE)
        self.append_column (c)
        c.set_sort_column_id (self.BOOK_COLUMN_TITLE)
        c = gtk.TreeViewColumn ("Sub Title", gtk.CellRendererText (), text=self.BOOK_COLUMN_SUBTITLE)
        c.set_visible (False)
        self.append_column (c)
        c = gtk.TreeViewColumn ("Authors", gtk.CellRendererText (), text=self.BOOK_COLUMN_AUTHOR_LIST)
        self.append_column (c)
        model = gtk.ListStore (gobject.TYPE_INT,    # OID
                               gobject.TYPE_STRING, # DEWEY
                               gobject.TYPE_STRING, # TITLE
                               gobject.TYPE_STRING, # SUBTITLE
                               gobject.TYPE_STRING) # AUTHOR_LIST
        result = library.db.query ("SELECT physical_book_id FROM physical_book").getresult()
        for (id,) in result:
            physical_book = self.library.get_object ('Library.PhysicalBook', id)
            iter = model.append ()
            title = physical_book.book.title
            if title == None: title = ""
            subtitle = physical_book.book.subtitle
            if subtitle == None: subtitle = ""
            author = ""
            for (person, job) in physical_book.book.person:
                name = person.get_full_name ()
                if name != "":
                    if author == "": author = name
                    else: author = author + ", " + name
            
            model.set (iter,
                       self.BOOK_COLUMN_ID, id,
                       self.BOOK_COLUMN_DEWEY, physical_book.book.dewey,
                       self.BOOK_COLUMN_TITLE, title,
                       self.BOOK_COLUMN_SUBTITLE, subtitle,
                       self.BOOK_COLUMN_AUTHOR_LIST, author)
        self.set_model (model)

class Overview:

    def on_bookworm_new_button_clicked (self, button):
        pass

    def __init__ (self, library):
        self.xml = gtk.glade.XML ('Overview.glade')
        self.library = library
        self.list = OverviewList (self.library)
        sw = self.xml.get_widget ('bookworm_swindow')
        sw.add (self.list)
        self.xml.signal_autoconnect (
            {'on_bookworm_new_button_clicked': self.on_bookworm_new_button_clicked,
             })
    def run (self):
        self.xml.get_widget ('bookworm_window').show_all ()
        gtk.mainloop()
    
if __name__ == '__main__':

    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()

    overview = Overview (library)
    overview.run ()
