import types

IntField = (types.IntType, 'int')
StringField = (types.StringType, 'string')
BoolField = (types.IntType, 'bool')
DateField = (types.StringType, 'date')
RealField = (types.FloatType, 'real')


def isbn_entry_insert_text_cb (entry, text, len, ignored):
    for l in text:
        if l not in '0123456789-X':
            entry.emit_stop_by_name ('insert-text')

def dewey_entry_insert_text_cb (entry, text, len, ignored):
    for l in text:
        if l not in '0123456789./':
            entry.emit_stop_by_name ('insert-text')

def year_entry_insert_text_cb (entry, text, len, ignored):
    for l in text:
        if l not in '0123456789':
            entry.emit_stop_by_name ('insert-text')
            
def int_entry_insert_text_cb (entry, text, len, ignored):
    for l in text:
        if l not in '0123456789':
            entry.emit_stop_by_name ('insert-text')
            
def float_entry_insert_text_cb (entry, text, len, ignored):
    for l in text:
        if l not in '0123456789.':
            entry.emit_stop_by_name ('insert-text')

def date_entry_insert_text_cb (entry, text, len, ignored):
    for l in text:
        if l not in '0123456789/-':
            entry.emit_stop_by_name ('insert-text')

def error_dialog (msg, widget):
    widget.grab_focus ()
    print msg

def generate_series_values (fields, values):
    field_string = ""
    value_string = ""
    assert (len (fields) == len (values))
    for i in range (0, len(fields)):
        if values[i] == '':
            continue
        if field_string == "":
            field_string = fields[i]
            value_string = "'" + values[i] + "'"
        else:
            field_string += ', ' + fields[i]
            value_string += ", '" + values[i] + "'"

    return (field_string, value_string)

def escape_string (value):
    value = value.replace ("\\", r"\\")
    value = value.replace ("'", r"\'")
    return value

def quote_value (field_type, value):
    if value == None:
        return "NULL"
    elif field_type == BoolField:
        if value:
            return 'true'
        return 'false'
    elif field_type == IntField or field_type == RealField:
        return str (value)
    elif field_type == StringField:
        if value == '':
            return "NULL"
        return "'%s'" % escape_string (str (value))
    else:
        return "'%s'" % str (value)

def test_dewey (value):
    digits = '1234567890'
    if len (value) < 3:
        return False
    for i in xrange (0, len (value)):
        if i == 3:
            if not value[i] == '.':
                return False
        elif not value[i] in digits:
            return False
            
    return True
            
def test_float (value):
    try:
        float (value)
    except ValueError:
        return False
    return True

def test_int (value):
    try:
        int (value)
    except ValueError:
        return False
    return True
