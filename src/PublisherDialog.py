import gobject
import gtk
import gtk.glade
import util
from OptionData import OptionData
from CityDialog import CityWidget

class PublisherWidget (OptionData):
    _klass_type = 'Library.Publisher'

    def __init__ (self, library, omenu, button = None, data=None, xml=None):
        if not xml:
            xml = gtk.glade.XML ("PublisherDialog.glade")
        OptionData.__init__ (self, library, omenu, button, data, xml)
        self.city = CityWidget (library,
                                self.xml.get_widget ('publisher_city_label'),
                                self.xml.get_widget ('publisher_new_button'),
                                parent=self.xml.get_widget ('publisher_dialog'))

    def _commit_dialog (self):
        OptionData._commit_dialog (self)
        publisher = self.get_object ()
        city = self.city.get_object ()

        if city:
            publisher.city = city

        buffer = self.xml.get_widget ('publisher_notes_text_view').get_buffer ()
        text = buffer.get_text (buffer.get_start_iter(), buffer.get_end_iter())
        if text:
            publisher.notes = text


class PublisherModel (gtk.ListStore):
    PUBLISHER_COLUMN_ID = 0
    PUBLISHER_COLUMN_NAME = 1

    def __init__ (self, library):
        self.library = library
        gtk.ListStore.__init__ (self,
                                gobject.TYPE_INT,    # ID
                                gobject.TYPE_STRING) # NAME
        result = self.library.db.query ("SELECT publisher_id FROM publisher").getresult()
        for (id,) in result:
            publisher = self.library.get_object ('Library.Publisher', id)
            iter = self.append ()
            self.set (iter,
                      self.PUBLISHER_COLUMN_ID, id,
                      self.PUBLISHER_COLUMN_NAME, publisher.name)
        self.set_sort_column_id (self.PUBLISHER_COLUMN_NAME, gtk.SORT_ASCENDING)
        

    def add_buttons (self, tree_view, add_button):
        self.tree_view = tree_view
        self.tree_view.set_search_column (self.PUBLISHER_COLUMN_NAME)
        add_button.connect ('clicked', self.add_publisher)

    def add_publisher (self, button):
        xml = gtk.glade.XML ("PublisherDialog.glade")
        publisher_dialog = xml.get_widget ('publisher_dialog')
        city_widget = CityWidget (self.library,
                           xml.get_widget ('publisher_city_label'),
                           xml.get_widget ('publisher_new_button'),
                           parent=publisher_dialog)
        result = publisher_dialog.run ()
        if result == gtk.RESPONSE_OK:
            entry = xml.get_widget ('publisher_dialog_entry')
            new_name = entry.get_text ()
            assert (new_name)
            oid = self.library.db.query ("INSERT INTO publisher (name) VALUES (%s)" % util.quote_value (util.StringField, new_name))
            result = self.library.db.query ("SELECT publisher_id FROM publisher WHERE oid = %d" % oid).getresult()[0]
            id = result[0]
            publisher = self.library.get_object ('Library.Publisher', id)

            city = city_widget.get_object ()

            if city:
                publisher.city = city

            buffer = xml.get_widget ('publisher_notes_text_view').get_buffer ()
            text = buffer.get_text (buffer.get_start_iter(), buffer.get_end_iter())
            if text:
                publisher.notes = text
            iter = self.append ()
            self.set (iter,
                      self.PUBLISHER_COLUMN_ID, id,
                      self.PUBLISHER_COLUMN_NAME, publisher.name)
            self.tree_view.get_selection ().select_iter (iter)
        publisher_dialog.destroy ()

    def get_object (self):
        (model, iter) = self.tree_view.get_selection ().get_selected ()
        if iter:
            id = model.get_value (iter, PublisherModel.PUBLISHER_COLUMN_ID)
            publisher = self.library.get_object ('Library.Publisher', id)
        else:
            publisher = None

        return publisher
