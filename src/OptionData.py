import gtk
import Library
import util

class OptionData (object):
    _klass_name = ''
    _klass_type = ''
    _field = 'name'

    def __init__ (self, library, omenu, button = None, data=None, xml=None):
        assert (library)
        self.library = library
        self.omenu = omenu
        self.button = button
        self.data = data
        self._setup_menu (data)
        if xml:
            self.xml = xml
        else:
            klass = self.library.get_class (self._klass_type)
            self.xml = gtk.glade.XML ("OptionData.glade", '%s_dialog' % klass._table_name)
        if button:
            self.button.connect ('clicked', self._new_item)

    def _setup_menu (self, data=None):
        klass = self.library.get_class (self._klass_type)
        query = "SELECT %s, %s FROM %s ORDER BY %s" % (self._field, klass._table_identifier[0], klass._table_name, self._field)
        self.current_list = self.library.db.query (query).getresult()
        if not self.current_list:
            self.omenu.set_sensitive (False)
        else:
            self.omenu.set_sensitive (True)
        menu = gtk.Menu()
        menu.append (gtk.MenuItem ("None"))
        history = 0
        j = 1
        for I in self.current_list:
            menu.append (gtk.MenuItem (I[0]))

            if data and data._id == I[1]:
                history = j
            j = j+1
        menu.show_all()
        self.omenu.set_menu (menu)
        self.omenu.set_history (history)

    def _new_item (self, button):
        klass = self.library.get_class (self._klass_type)
        dialog = self.xml.get_widget ('%s_dialog' % klass._table_name)
        dialog.set_transient_for (button.get_toplevel ())
        entry = self.xml.get_widget ('%s_dialog_entry' % klass._table_name)
        entry.set_text ('')
        while 1:
            button = dialog.run ()
            if not button == gtk.RESPONSE_OK:
                dialog.hide()
                return False
            if self.check_dialog ():
                self._commit_dialog ()
                dialog.hide ()
                return True

    def check_dialog (self):
        klass = self.library.get_class (self._klass_type)
        entry = self.xml.get_widget ('%s_dialog_entry' % klass._table_name)
        new_name = entry.get_text ()
        if new_name == '':
            util.error_dialog ("FIXME: real error", entry)
            return False
        for I in self.current_list:
            if I[0] == new_name:
                util.error_dialog ("FIXME: real error", entry)
                return False
        return True

    def _commit_dialog (self):
        klass = self.library.get_class (self._klass_type)
        entry = self.xml.get_widget ('%s_dialog_entry' % klass._table_name)
        new_name = entry.get_text ()
        oid = self.library.db.query ("INSERT INTO %s (%s) VALUES (%s)"% (klass._table_name, self._field, util.quote_value (util.StringField, new_name)))
        result = self.library.db.query ("SELECT %s FROM %s WHERE oid = %d" % (klass._table_identifier[0], klass._table_name, oid)).getresult()[0]
        obj = self.library.get_object (self._klass_type, result[0])
        self._setup_menu (obj)

    def set_object (self, object):
        menu = self.omenu
        if object == None:
            menu.set_history (0)
            return
        i = 1
        for I in self.current_list:
            if I[1] == object._id:
                menu.set_history (i)
                return
            i = i + 1
        menu.set_history (0)
        
    def get_object (self):
        history = self.omenu.get_history()
        if history == 0:
            return None
        return self.library.get_object (self._klass_type, self.current_list[history - 1][1])
    

class SuffixWidget (OptionData):
    _klass_type = 'Library.Suffix'

class HonorificWidget (OptionData):
    _klass_type = 'Library.Honorific'

class PersonalTitleWidget (OptionData):
    _klass_type = 'Library.PersonalTitle'

class NationalityWidget (OptionData):
    _klass_type = 'Library.Nationality'

class LanguageWidget (OptionData):
    _klass_type = 'Library.Languages'

class FormatWidget (OptionData):
    _klass_type = 'Library.Format'

class CurrencyWidget (OptionData):
    _klass_type = 'Library.Currency'

class TypeWidget (OptionData):
    _klass_type = 'Library.Type'

class LocationWidget (OptionData):
    _klass_type = 'Library.Location'

class BookstoreTopicsWidget (OptionData):
    _klass_type = 'Library.BookstoreTopics'
    _field = 'topic'
