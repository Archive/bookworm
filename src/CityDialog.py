import gobject
import gtk
import gtk.glade
import Library
import util
from gettext import gettext as _



class CityWidget (object):
    def __init__ (self, library, label, button, city=None, parent=None):
        assert (label != None)
        assert (button != None)
        self.library = library
        self.xml = gtk.glade.XML ('CityDialog.glade', 'select_city_dialog')
        self.label = label
        self.button = button
        self.set_object (city)
        self.button.connect ('clicked', self.select_city)
        self.xml.get_widget ('select_city_new_button').connect ('clicked', self.new_city_button)
        self.model = None
        tree_view = self.xml.get_widget ('select_city_tree_view')
        tree_view.get_selection ().set_mode (gtk.SELECTION_BROWSE)
        column = gtk.TreeViewColumn (None, gtk.CellRendererText(), text=0)
        tree_view.append_column (column)
        if parent:
            self.xml.get_widget ('select_city_dialog').set_transient_for (parent)

    def new_city_button (self, widget):
        new_city_dialog = CityDialog(self.library)
        city = new_city_dialog.run ()
        if city == None:
            return
        self.city = city
        self._setup_tree (city)

    def select_city (self, widget):
        dialog = self.xml.get_widget ('select_city_dialog')
        self._setup_tree (self.city)
        while 1:
            button = dialog.run ()
            if button == gtk.RESPONSE_OK:
                tree_view = self.xml.get_widget ('select_city_tree_view')
                (model, iter) = tree_view.get_selection ().get_selected ()
                id = model.get_value (iter, 1)
                city = self.library.get_object ('Library.City', id)
                if city:
                    self.city = city
                if self.city:
                    self.label.set_text (str (city))
                else:
                    self.label.set_text (_("No city selected"))
            dialog.hide ()
            return

    def get_object (self):
        return self.city

    def set_object (self, city):
        self.city = city
        if self.city:
            self.label.set_text (str (city))
        else:
            self.label.set_markup ("<i>%s</i>" % _("No city selected"))


    def _setup_tree (self, city):
        tree_view = self.xml.get_widget ('select_city_tree_view')
        self.model = gtk.TreeStore (gobject.TYPE_STRING, gobject.TYPE_INT)
        tree_view.set_model (self.model)
        cities = self.library.db.query ('SELECT country, state, name, city_id FROM city ORDER BY country, state, name').getresult()
        current_country = None
        current_state = None

        # pretty ugly code.
        # assumes that it's all sorted correctly, and that NULL comes at the end
        (city_iter, state_iter, country_iter) = (None, None, None)
        select_iter = None
        if city:
            id = city._id
        else:
            id = None
        for city_info in cities:
            if city_info[0] == None:
                continue
#FIXME: uncomment this and remove the above when we have a rational database
#            assert city_info[0]
            if city_info [0] == current_country:
                #we're the same country
                if city_info[1] == current_state:
                    #we're in the same state:
                    if city_info[2] == None:
                        #we're in the last row in this area
                        if current_state == None:
                            self.model.set_value (country_iter, 1, city_info[3])
                            if id == city_info[3]: select_iter = country_iter
                        else:
                            self.model.set_value (state_iter, 1, city_info[3])
                            if id == city_info[3]: select_iter = state_iter
                    else:
                        city_iter = self.model.insert_after (None, city_iter)
                        self.model.set_value (city_iter, 0, city_info[2])
                        self.model.set_value (city_iter, 1, city_info[3])
                        if id == city_info[3]: select_iter = city_iter
                else:
                    if city_info[1]:
                        state_iter = self.model.insert_after (None, state_iter)
                        self.model.set_value (state_iter, 0, city_info[1])
                        if city_info[2] == None:
                            self.model.set_value (state_iter, 1, city_info[3])
                            if id == city_info[3]: select_iter = state_iter
                        else:
                            city_iter = self.model.insert_after (state_iter, None)
                            self.model.set_value (city_iter, 0, city_info[2])
                            self.model.set_value (city_iter, 1, city_info[3])
                            if id == city_info[3]: select_iter = city_iter
                    else:
                        self.model.set_value (country_iter, 1, city_info[3])
                        if id == city_info[3]: select_iter = country_iter
            else:
                #we're in a totally new country:
                country_iter = self.model.append (None)
                self.model.set_value (country_iter, 0, city_info[0])
                if city_info[1] == None and city_info[2] == None:
                    self.model.set_value (country_iter, 1, city_info[3])
                    if id == city_info[3]: select_iter = country_iter
                elif city_info[1] == None:
                    city_iter = self.model.append (country_iter)
                    self.model.set_value (city_iter, 0, city_info[2])
                    self.model.set_value (city_iter, 1, city_info[3])
                    if id == city_info[3]: select_iter = city_iter
                elif city_info[2] == None:
                    state_iter = self.model.append (country_iter)
                    self.model.set_value (state_iter, 0, city_info[1])
                    self.model.set_value (state_iter, 1, city_info[3])
                    if id == city_info[3]: select_iter = state_iter
                else:
                    state_iter = self.model.append (country_iter)
                    self.model.set_value (state_iter, 0, city_info[1])
                    city_iter = self.model.append (state_iter)
                    self.model.set_value (city_iter, 0, city_info[2])
                    self.model.set_value (city_iter, 1, city_info[3])
                    if id == city_info[3]: select_iter = city_iter

            current_country = city_info[0]
            current_state = city_info[1]
        if select_iter == None:
            select_iter = self.model.get_iter_first ()
        tree_view.expand_all ()
        path = self.model.get_path(select_iter)
        tree_view.set_cursor (path, tree_view.get_column(0), False)
        tree_view.scroll_to_cell (path, tree_view.get_column(0), True, 0.5, 0.0)

class CityDialog (object):
    def __init__ (self, library, parent=None):
        self.library = library
        self.xml = gtk.glade.XML ('CityDialog.glade', 'city_dialog')
        if parent:
            self.xml.get_widget ('city_dialog').set_transient_for (parent)

    def run (self, city=None):
        dialog = self.xml.get_widget ('city_dialog')
        city_entry = self.xml.get_widget ('city_dialog_city_entry')
        state_entry = self.xml.get_widget ('city_dialog_state_entry')
        country_entry = self.xml.get_widget ('city_dialog_country_entry')

        city_entry.set_text('')
        state_entry.set_text('')
        country_entry.set_text('')
        if city == None:
            # we're making a new one
            dialog.set_title (_("Create a new city"))
        else:
            # we're editing a current city
            dialog.set_title (_("Edit city"))

            if city.name:
                city_entry.set_text(city.name)
            if city.state:
                state_entry.set_text(city.state)
            if city.country:
                country_entry.set_text(city.country)
        while 1:
            button = dialog.run ()
            retval = None
            if button == gtk.RESPONSE_OK:
                city_text = city_entry.get_text()
                state_text = state_entry.get_text()
                country_text = country_entry.get_text()
                (field_string, value_string) = \
                               util.generate_series_values (('name', 'state', 'country'), (city_text, state_text, country_text))
                if field_string == '':
                    util.error_dialog (_(""))
                    continue
                query = "SELECT field_string "
                if city == None:
                    query = "INSERT INTO city (" + field_string + ") VALUES (" + value_string + ")"
                    oid = self.library.db.query (query)
                    result = self.library.db.query ("SELECT city_id FROM city WHERE oid = %d" % oid).getresult()
                    city = self.library.get_object ("Library.City", result[0][0])
                    retval = city
                else:
                    city.name = city_text
                    city.state = state_text
                    city.country = country_text
                    retval = city
            dialog.destroy()
            return retval

if __name__ == '__main__':
    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()
    if len (sys.argv) == 2:
        id = sys.argv[1]
        city = library.get_object ("Library.City", id)
    else:
        city = None

    dialog = CityDialog (library)
    city = dialog.run (city)
    if city:
        print city._id
    
