import gtk
import gobject
import Library
from rhpl.GenericError import *


class PersonModel(gtk.ListStore):
    PERSON_COLUMN_ID = 0
    PERSON_COLUMN_NAME = 1

    def __init__ (self, library):
        self.library = library
        gtk.ListStore.__init__ (self,
                                gobject.TYPE_INT,    # ID
                                gobject.TYPE_STRING) # NAME
        result = self.library.db.query ("SELECT person_id, first_name, middle_name, last_name, honorifics_id, suffix_id, personal_title_id FROM person").getresult()
        for (id, first_name, middle_name, last_name, honorific_id, suffix_id, personal_title_id) in result:
            suffix = self.library.get_object ('Library.Suffix', suffix_id)
            honorific = self.library.get_object ('Library.Honorific', honorific_id)
            personal_title = self.library.get_object ('Library.PersonalTitle', personal_title_id)
            iter = self.append ()
            formal_name = Library.construct_formal_name(first_name, middle_name, last_name, honorific, suffix, personal_title)
            self.set (iter,
                      self.PERSON_COLUMN_ID, id,
                      self.PERSON_COLUMN_NAME, formal_name)
        self.set_sort_column_id (self.PERSON_COLUMN_NAME, gtk.SORT_ASCENDING)

    def add_person (self, person):
        iter = self.append()
        self.set (iter,
                  self.PERSON_COLUMN_ID, person.person_id,
                  self.PERSON_COLUMN_NAME, person.get_formal_name ())
        path = self.get_path (iter)
        return path
