import gtk
import gtk.glade


class NotesDialog:

    def __init__ (self, notes=None, parent = None):
        self.xml = gtk.glade.XML ('NotesDialog.glade')
        self.notes = notes
        self.parent = parent

    def run (self):
        dialog = self.xml.get_widget ('notes_dialog')
        buffer = self.xml.get_widget ('notes_text_view').get_buffer ()
        retval = False

        if self.parent:
            dialog.set_transient_for (self.parent)
        if self.notes:
            buffer.set_text (self.notes)
        button = dialog.run ()
        if button == gtk.RESPONSE_OK:
            notes = buffer.get_text (buffer.get_start_iter(), buffer.get_end_iter())
            self.notes = notes
            retval = True
        dialog.destroy ()
        return retval


class NotesWidget:
    def __init__ (self, button, notes=None):
        self.notes = notes
        self.button = button
        self.button.connect ('clicked', self._set_notes)
        self.parent = button.get_toplevel ()

    def _set_notes (self, button):
        dialog = NotesDialog (self.notes, self.parent)
        if dialog.run():
            self.notes = dialog.notes


    def get_object (self):
        return self.notes
