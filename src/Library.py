#!/opt/gnome2/bin/python -i

# Library.py
# Ideas blatantly ripped from distobject.py by Sopwith

from config import DBNAME, HOST, PORT

True=1
False=0

import pg
import util
from util import IntField
from util import StringField
from util import BoolField
from util import DateField
from util import RealField



def get_persistent_joins(klass, x=None):
    if x is None:
        x = {}
    if hasattr(klass, '_joins'):
        x.update(klass._joins)
#    for I in klass.__bases__:
#        get_persistent_joins (I, x)
    return x



class ReferentialIntegrityViolation (Exception):
    def __init__ (self, args):
        self.args = args
    
class Library:

    class_list = {}

    def __init__(self):
        self.db = None
        self.obj_cache = {}

    def connect (self):
        self.db = pg.connect (dbname=DBNAME, host=HOST, port=PORT)
        #self.db = pg.connect (dbname=DBNAME)

    def get_class (self, klass_name):
        if klass_name in Library.class_list:
            return Library.class_list[klass_name]
        return None

    def get_object (self, klass_name, id):
        if id == None or id == 'None':
            return None
        klass = Library.class_list[klass_name]
        try:
            klass_cache = self.obj_cache[klass_name]
        except KeyError:
            klass_cache = {}
            self.obj_cache[klass_name] = klass_cache

        if id in klass_cache:
            return klass_cache[id]

        obj = klass (self, id)
        klass_cache[id] = obj

        return obj

    def create_object (self, klass_name, **kwargs):
        klass = Library.class_list[klass_name]
        fields = ''
        values = ''
        field_attributes = klass._field_info.keys ()
        join_attributes = klass._joins.keys ()
        field_values = []
        join_values = []
        # First, make sure that the values passed in to create object are valid ones:
        for val in kwargs.keys ():
            if val in field_attributes:
                field_values.append (val)
            elif val in join_attributes:
                join_values.append (val)
            else:
                raise KeyError

        # Next, we create the object.
        if field_values == []:
            raise KeyError
        fields = ', '.join (field_values)
        values = ', '.join (map (lambda val: util.quote_value (klass._field_info [val], kwargs[val]), field_values))
        #FIXME: Bad hack until I let you create physical books w/o a book
        if klass._klass_name == 'Library.PhysicalBook' and 'book' in join_values:
            fields = fields + ', book_id'
            val = kwargs['book']
            if isinstance (val, Record):
                id = util.quote_value (IntField, val._id)
            else:
                id = util.quote_value (IntField, val)
            values = values + ', ' + id
            
        if fields == '':
            oid = self.db.query ("INSERT into %s" % (klass._table_name))
        else:
            oid = self.db.query ("INSERT into %s (%s) VALUES (%s)" % (klass._table_name, fields, values))
        list = self.db.query ("SELECT %s FROM %s WHERE oid=%d" % (klass._table_identifier[0], klass._table_name, oid) ).getresult()
        assert len (list) == 1
        (id, ) = list[0]

        object = self.get_object (klass_name, id)
        for val in join_values:
            object.__setattr__ (val, kwargs[val])
        return object 

    def delete_object (self, obj, break_links=0):
        # First, let's check to see if anything refers to obj
        id_string = "%s = %s" % (obj._table_identifier[0], util.quote_value (obj._table_identifier[1], obj._id))
        linked_objects = []
        for klass in Library.class_list.values():
            if obj._klass_name == klass._klass_name:
                continue
            for join in klass._joins.keys():
                join_info = klass._joins [join]
                if join_info[1] == obj._klass_name:
                    id_string = "%s = %s" % (join_info[0], util.quote_value (obj._table_identifier[1], obj._id))
                    #found a klass that might link to it.  Let's check
                    if join_info[3]:
                        query = "SELECT %s FROM %s WHERE %s" % (klass._table_identifier[0], join_info[4], id_string)
                    else:
                        query = "SELECT %s FROM %s WHERE %s" % (klass._table_identifier[0], klass._table_name, id_string)
                    results = self.db.query (query).getresult()
                    for (result_id, ) in results:
                        linking_object = self.get_object (klass._klass_name, result_id)
                        if break_links:
                            linking_object.__setattr__ (join, None)
                        else:
                            linked_objects.append (linking_object)
        if not linked_objects == []:
            raise ReferentialIntegrityViolation (linked_objects)
                    
        # we're probably safe to delete it, then.
        self.db.query ("DELETE FROM %s WHERE %s=%s" % (obj._table_name, obj._table_identifier[0], util.quote_value (obj._table_identifier[1], obj._id)))
        obj._destroyed = True

        klass_cache = self.obj_cache[obj._klass_name]
        del klass_cache [obj._id]

        
    def begin_transaction (self):
        self.db.query ("BEGIN WORK")

    def cancel_transaction (self):
        self.db.query ("ROLLBACK WORK")
        
    def commit_transaction (self):
        self.db.query ("COMMIT WORK")

class Record (object):
    _klass_name = 'Library.Record'
    _joins = {}
    _table_identifier = ('', None)
    _table_name = ''
    _field_info = {}
    _natural_order = ''

    def __init__ (self, library, id):
        assert (id)
        self._library = library
        self._row = None
        self._id = id
        self._destroyed = False
        self.update_row ()

    def __getattr__ (self, attr):
        if attr == None: raise KeyError
        if attr[0] == '_': return self.__dict__[attr]

        if self._row == None: self.update_row ()

        if attr in self._joins.keys ():
            joins = get_persistent_joins (self)
            if joins.has_key(attr):
                jrec = joins[attr]
                if jrec[3]:
                    klass_list = []
                    if type (jrec[1]) == type (tuple()):
                        for klass_name in jrec[1]:
                            klass_list.append (self._library.get_class (klass_name))
                        identifiers = ', '.join (map (lambda x: x._table_identifier[0], klass_list))
                    else:
                        klass_list.append (self._library.get_class (jrec[1]))
                        identifiers = jrec[0]
                    query = "SELECT %s FROM %s WHERE " % (identifiers, jrec[4])
                    query = query + "%s = %s" % (self._table_identifier[0], util.quote_value (self._table_identifier[1], self._id))
                    list = self._library.db.query (query).getresult()
                    ret_list = []
                    for result in list:
                        joins = []
                        assert len (klass_list) == len (result)
                        for i in range (0, len (result)):
                            join_obj = self._library.get_object (klass_list[i]._klass_name, str (result[i]))
                            joins.append (join_obj)
                        if len (joins) == 1:
                            ret_list.append (joins[0])
                        else:
                            ret_list.append (tuple (joins))
                    return ret_list
                
                obj_id = self._row[jrec[0]]
                if obj_id == None:
                    return None
                klass_name = jrec[1]
                return self._library.get_object (klass_name, obj_id)
            else:
                raise AttributeError, "Attribute `%s' not found." % attr

        return self._row[attr]

    def __setattr__ (self, attr, value):
        if attr == None: raise KeyError
        if attr[0] == '_':
            self.__dict__[attr] = value
            return
        if self._row == None: self.update_row()

        if attr in self._field_info.keys ():
            valtype = self._field_info[attr][0]
            if value and not isinstance(value, valtype):
                raise TypeError, "Invalid type"
            self._row[attr] = value

            query = "UPDATE %s SET %s = %s WHERE %s = %s" % \
                    (self._table_name, attr, util.quote_value (self._field_info[attr], value),
                     self._table_identifier[0], util.quote_value (self._table_identifier[1], self._id))
            self._library.db.query (query)
        elif attr in self._joins.keys ():
            joins = get_persistent_joins (self)
            jrec = joins[attr]

            if jrec[3]:
                print 'Setting joins is not supported.'
                return None
                klass_list = []
                table = jrec[4]
                if type (jrec[1]) == type (tuple()):
                    for klass_name in jrec[1]:
                        klass_list.append (self._library.get_class (klass_name))
                    identifiers = ', '.join (map (lambda x: x._table_identifier[0], klass_list))
                else:
                    klass_list.append (self._library.get_class (jrec[1]))
                    identifiers = jrec[0]
            else:
                table = self._table_name
                identifiers = jrec[0]
                if value == None:
                    values = 'NULL'
                    self._row[jrec[0]] = None
                elif isinstance (value, Record):
                    values = util.quote_value (self._joins[attr][2], value._id)
                    self._row[jrec[0]] = value._id
                else:
                    values = util.quote_value (self._joins[attr][2], value)
                    self._row[jrec[0]] = value
            query = "UPDATE %s SET %s = %s WHERE %s = %s" % \
                    (table, identifiers, values, self._table_identifier[0], util.quote_value (self._table_identifier[1], self._id))
            self._library.db.query (query)
        else:
            raise KeyError

    def __del__ (self):
        if self._destroyed:
            return
        self.library.delete_object (self)

    def update_row (self):
        query = "SELECT * FROM %s WHERE %s=%s" % (self._table_name, self._table_identifier[0],
                                                  util.quote_value (self._table_identifier[1], self._id))
        row_result = self._library.db.query (query).dictresult()
        if len (row_result) == 0:
            raise KeyError
        assert (len (row_result) == 1)
        self._row = row_result[0]

class Series (Record):
    _klass_name = 'Library.Series'
    _table_identifier = ('series_id', IntField)
    _table_name = 'series'
    _field_info = {'name' : StringField, 'length' : IntField, 'notes' : StringField }

Library.class_list[Series._klass_name] = Series

class PublisherSeries (Record):
    _klass_name = 'Library.PublisherSeries'
    _table_identifier = ('publisher_series_id', IntField)
    _table_name = 'publisher_series'
    _joins = {'publisher' : ('publisher_id', 'Library.Publisher', IntField, 0) }
    _field_info = {'name' : StringField, 'notes' : StringField }

Library.class_list[PublisherSeries._klass_name] = PublisherSeries

class Languages (Record):
    _klass_name = 'Library.Languages'
    _table_identifier = ('language_id', StringField)
    _table_name = 'languages'
    _field_info = {'name' : StringField }

Library.class_list[Languages._klass_name] = Languages


class Book (Record):
    _klass_name = 'Library.Book'
    _table_identifier = ('book_id', IntField)
    _table_name = 'book'

    _joins = {'original_language' : ('original_language_id', 'Library.Languages', StringField, 0),
              'series' : ('series_id', 'Library.Series', IntField, 0),
              'subject' : ('subject_id', 'Library.Subject', IntField, 1, 'book_subject_link'),
              'person': ('person_id', ('Library.Person','Library.Jobs'), IntField, 1, 'book_person_link')
              }
    _field_info = { 'title' : StringField, 'subtitle' : StringField, 'alternate_names' : StringField,
                    'dewey' : StringField, 'copyright_date' : IntField, 'biography' : BoolField, 'fiction' : BoolField,
                    'notes' : StringField, 'series_location' : IntField }

Library.class_list[Book._klass_name] = Book


class Location (Record):
    _klass_name = 'Library.Location'
    _table_identifier = ('location_id', IntField)
    _table_name = 'location'
    _field_info = {'name' : StringField }

Library.class_list[Location._klass_name] = Location

class City (Record):
    _klass_name = 'Library.City'
    _table_identifier = ('city_id', IntField)
    _table_name = 'city'
    _field_info = {'name' : StringField, 'state' : StringField, 'country' : StringField }

    def __str__ (self):
        s = ''
        if self.name:
            s = self.name
        if self.state:
            if s: s = "%s, %s" % (s, self.state)
            else: s = self.state
        if self.country:
            if s: s = "%s, %s" % (s, self.country)
            else: s = self.country
        return s
        
Library.class_list[City._klass_name] = City

class Publisher (Record):
    _klass_name = 'Library.Publisher'
    _table_identifier = ('publisher_id', IntField)
    _table_name = 'publisher'
    _field_info = {'name' : StringField, 'notes' : StringField }
    _joins = {'city' : ('city_id', 'Library.City', IntField, 0) }

Library.class_list[Publisher._klass_name] = Publisher


class BookstoreTopics (Record):
    _klass_name = 'Library.BookstoreTopics'
    _table_identifier = ('bookstore_topics_id', IntField)
    _table_name = 'bookstore_topics'
    _field_info = {'topic' : StringField }

Library.class_list[BookstoreTopics._klass_name] = BookstoreTopics


class Format (Record):
    _klass_name = 'Library.Format'
    _table_identifier = ('format_id', IntField)
    _table_name = 'format'
    _field_info = {'name' : StringField }

Library.class_list[Format._klass_name] = Format

class Currency (Record):
    _klass_name = 'Library.Currency'
    _table_identifier = ('currency_id', IntField)
    _table_name = 'currency'
    _field_info = {'name' : StringField }

Library.class_list[Currency._klass_name] = Currency

class Type (Record):
    _klass_name = 'Library.Type'
    _table_identifier = ('type_id', IntField)
    _table_name = 'type'
    _field_info = {'name' : StringField }

Library.class_list[Type._klass_name] = Type

class PhysicalBook (Record):
    _klass_name = 'Library.PhysicalBook'
    _table_identifier = ('physical_book_id', IntField)
    _table_name = 'physical_book'
    _joins = {'language': ("language_id", 'Library.Languages', StringField, 0),
              'format' : ("format_id", 'Library.Format', IntField, 0),
              'currency' : ("currency_id", 'Library.Currency', IntField, 0),
              'location' : ('location_id', 'Library.Location', IntField, 0),
              'publisher' :('publisher_id', 'Library.Publisher', IntField, 0),
              'publisher_series' :('publisher_series_id', 'Library.PublisherSeries', IntField, 0),
              'publication_city' :('publication_city_id', 'Library.City', IntField, 0),
              'book' : ('book_id', 'Library.Book', IntField, 0),
              'bookstore_topics' : ('bookstore_topics_id', 'Library.BookstoreTopics', IntField, 0),
              'person': ('person_id', ('Library.Person','Library.Jobs'), IntField, 1, 'physicalbook_person_link'),
              'type': ('type_id', 'Library.Type', IntField, 0)
              }
    _field_info = {'isbn':StringField, 'edition':IntField, 'printing':IntField, 'price':StringField,
                   'autographed':BoolField, 'number_pages':IntField, 'length':RealField,
                   'width':RealField, 'height':RealField, 'date_acquired':DateField, 'notes':StringField
                  }


Library.class_list[PhysicalBook._klass_name] = PhysicalBook


class AudioBook (Record):
    _klass_name = 'Library.AudioBook'
    _table_identifier = ('audio_book_id', IntField)
    _table_name = 'audio_book'
    _joins = {'language': ("language_id", 'Library.Languages', StringField, 0),
              'format' : ("format_id", 'Library.Format', IntField, 0),
              'location' : ('location_id', 'Library.Location', IntField, 0),
              'publisher' :('publisher_id', 'Library.Publisher', IntField, 0),
              'book' : ('book_id', 'Library.Book', IntField, 0),
              'bookstore_topics' : ('bookstore_topics_id', 'Library.BookstoreTopics', IntField, 0),
              'person': ('person_id', ('Library.Person','Library.Jobs'), IntField, 1, 'audiobook_person_link')
              }
    _field_info = {'price' : StringField, 'duration' : IntField,
                   'size' : IntField, 'date_acquired' : DateField,
                   'notes':StringField
                   }

Library.class_list[AudioBook._klass_name] = AudioBook


class EBook (Record):
    _klass_name = 'Library.EBook'
    _table_identifier = ('e_book_id', IntField)
    _table_name = 'e_book'
    _joins = {'language': ("language_id", 'Library.Languages', StringField, 0),
              'location' : ('location_id', 'Library.Location', IntField, 0),
              'publisher' :('publisher_id', 'Library.Publisher', IntField, 0),
              'book' : ('book_id', 'Library.Book', IntField, 0),
              'person': ('person_id', ('Library.Person','Library.Jobs'), IntField, 1, 'ebook_person_link')
              }
    _field_info = {'price' : StringField, 'duration' : IntField,
                   'notes':StringField
                   }

Library.class_list[EBook._klass_name] = EBook


class Honorific (Record):
    _klass_name = 'Library.Honorific'
    _table_identifier = ('honorifics_id', IntField)
    _table_name = 'honorifics'
    _field_info = {'name' : StringField }

Library.class_list[Honorific._klass_name] = Honorific

class PersonalTitle (Record):
    _klass_name = 'Library.PersonalTitle'
    _table_identifier = ('personal_title_id', IntField)
    _table_name = 'personal_title'
    _field_info = {'name' : StringField }

Library.class_list[PersonalTitle._klass_name] = PersonalTitle

class Suffix (Record):
    _klass_name = 'Library.Suffix'
    _table_identifier = ('suffix_id', IntField)
    _table_name = 'suffix'
    _field_info = {'name' : StringField }

Library.class_list[Suffix._klass_name] = Suffix


class Nationality (Record):
    _klass_name = 'Library.Nationality'
    _table_identifier = ('nationality_id', IntField)
    _table_name = 'nationality'
    _field_info = {'name' : StringField }

Library.class_list[Nationality._klass_name] = Nationality


def construct_full_name (first_name, middle_name, last_name, honorific, suffix):
    name = ""
    if first_name:
        if name == "":
            name = first_name
        else:
            name = name + " " + first_name
    if middle_name and middle_name != 'FIXME':
        if name == "":
            name = middle_name
        else:
            name = name + " " + middle_name
    if last_name:
        if name == "":
            name = last_name
        else:
            name = name + " " + last_name
    if suffix:
        if name != "":
            name = name + " " + last_name
    if honorific and name != "":
        name = name + ", " + honorific.name
    return name    

def construct_formal_name (first_name, middle_name, last_name, honorific, suffix, personal_title):
    name = ""
    if last_name:
        name = last_name
    if first_name:
        if personal_title:
            if name:
                name = name + ", " + personal_title.name + ' ' + first_name
            else:
                name = personal_title.name + ' ' + first_name
        else:
            if name:
                name = name + ", " + first_name
            else:
                name = first_name
    if middle_name:
        if name:
            name = name + " " + middle_name
        else:
            name = middle_name
    if suffix:
        if name:
            name = name + " " + suffix.name
    if honorific and name != "":
        name = name + ", " + honorific.name
    return name    

class Person (Record):
    _klass_name = 'Library.Person'
    _table_identifier = ('person_id', IntField)
    _table_name = 'person'
    _joins = {'nationality' : ('nationality_id', 'Library.Nationality', IntField, 0),
              'suffix' : ('suffix_id', 'Library.Suffix', IntField, 0),
              'honorific' : ('honorifics_id', 'Library.Honorific', IntField, 0),
              'personal_title' : ('personal_title_id', 'Library.PersonalTitle', IntField, 0),
              'residence' : ('residence_id', 'Library.City', IntField, 0),
              'birth_city' : ('birth_city_id', 'Library.City', IntField, 0)
              }
    _field_info = {'last_name' : StringField, 'middle_name' : StringField,
                   'first_name' : StringField, 'birth_year' : IntField,
                   'birth_date' : DateField, 'death_year' : IntField,
                   'death_date' : DateField, 'occupation' : StringField,
                   'notes':StringField
                   }
    _natural_order = 'ORDER BY last_name, first_name, middle_name'



    def get_full_name (self):
        return construct_full_name (self.first_name, self.middle_name, self.last_name, self.honorific, self.suffix)
        
    def get_formal_name (self):
        return construct_formal_name (self.first_name, self.middle_name, self.last_name, self.honorific, self.suffix, self.personal_title)

Library.class_list[Person._klass_name] = Person


class Subject (Record):
    _klass_name = 'Library.Subject'
    _table_identifier = ('subject_id', IntField)
    _table_name = 'subject'
    _field_info = {'name' : StringField }
    _natural_order = 'ORDER BY name'

Library.class_list[Subject._klass_name] = Subject


class Jobs (Record):
    _klass_name = 'Library.Jobs'
    _table_identifier = ('jobs_id', IntField)
    _table_name = 'jobs'
    _field_info = {'name' : StringField }

Library.class_list[Jobs._klass_name] = Jobs


if __name__ == '__main__':
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library()
    library.connect()
