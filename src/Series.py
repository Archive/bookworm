import gtk
import gobject
from rhpl.GenericError import *


def sort_func (model, iter1, iter2):
    id1 = model.get_value(iter1, SeriesModel.SERIES_COLUMN_ID)
    id2 = model.get_value(iter2, SeriesModel.SERIES_COLUMN_ID)
    if id1 == -1:
        return -1
    elif id2 == -1:
        return 1

    str1 = model.get_value(iter1, SeriesModel.SERIES_COLUMN_NAME)
    str2 = model.get_value(iter2, SeriesModel.SERIES_COLUMN_NAME)

    if str1 < str2:
        return -1
    elif str1 > str2:
        return 1
    return 0

class SeriesModel(gtk.ListStore):
    SERIES_COLUMN_ID = 0
    SERIES_COLUMN_NAME = 1
    SERIES_COLUMN_LENGTH = 2

    def __init__ (self, library):
        self.library = library
        gtk.ListStore.__init__ (self,
                                gobject.TYPE_INT,    # ID
                                gobject.TYPE_STRING, # NAME
                                gobject.TYPE_STRING) # LENGTH
        result = self.library.db.query ("SELECT series_id, name, length FROM series").getresult()
        iter = self.append()
        self.set (iter,
                  self.SERIES_COLUMN_ID, -1,
                  self.SERIES_COLUMN_NAME, 'None',
                  self.SERIES_COLUMN_LENGTH, '')
        for (id, name, length) in result:
            series = self.library.get_object ('Library.Series', id)
            iter = self.append ()
            self.set (iter,
                      self.SERIES_COLUMN_ID, id,
                      self.SERIES_COLUMN_NAME, name,
                      self.SERIES_COLUMN_LENGTH, str (length))
        self.set_sort_column_id (self.SERIES_COLUMN_NAME, gtk.SORT_ASCENDING)
        self.set_sort_func (self.SERIES_COLUMN_NAME, sort_func)


    def add_series (self, series):
        iter = self.append()
        self.set (iter,
                  self.SERIES_COLUMN_ID, series.series_id,
                  self.SERIES_COLUMN_NAME, series.name,
                  self.SERIES_COLUMN_LENGTH, series.length)
        path = self.get_path (iter)
        return path
