import gtk
import gobject

TOGGLE_WIDTH = 12

class BookCellRenderer (gtk.GenericCellRenderer):
        property_names = ('text','value_type',)
        __gproperties__ = {
                'text': (gobject.TYPE_STRING, 'text', 'text displayed by the cell',
                         '', gobject.PARAM_READWRITE),
                'value_type': (gobject.TYPE_STRING, 'value_type', 'value type of the cell',
                               '', gobject.PARAM_READWRITE),
        }
        __gsignals__ = {
                'value-changed': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
                                  (gobject.TYPE_STRING, gobject.TYPE_STRING))
        }
    
    def __init__(self):
        self.__gobject_init__()
        self.xpad = -2; self.ypad = -2
        self.xalign = 0.5; self.yalign = 0.5
        self.active = 0
        
    def on_render(self, window, widget, background_area,
                  cell_area, expose_area, flags):

        x_offset, y_offset, width, height = self.on_get_size(widget, cell_area)
        width -= self.xpad*2
        height -= self.ypad*2

#        if width <= 0 or height <= 0:
#            return

        widget.style.paint_check(window,
                                 gtk.STATE_ACTIVE, gtk.SHADOW_IN, 
                                 cell_area, widget, "cellradio",
                                 cell_area.x + x_offset + self.xpad,
                                 cell_area.y + y_offset + self.ypad,
                                 width - 1,  height - 1)
                             
    def on_get_size(self, widget, cell_area):
        calc_width = self.xpad * 2 + TOGGLE_WIDTH
        calc_height = self.ypad * 2 + TOGGLE_WIDTH
        
        if cell_area:
            x_offset = self.xalign * (cell_area.width - calc_width)
            x_offset = max(x_offset, 0)
            y_offset = self.yalign * (cell_area.height - calc_height)
            y_offset = max(y_offset, 0)            
        else:
            x_offset = 0
            y_offset = 0
            
        return calc_width, calc_height, x_offset, y_offset
gobject.type_register(BookCellRenderer)
