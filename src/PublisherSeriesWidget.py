import gtk
import gtk.glade
import gobject
import util

from PublisherDialog import PublisherWidget
from TestError import *
import Library


def sort_func (model, iter1, iter2):
    id1 = model.get_value(iter1, PublisherSeriesModel.PUBLISHER_SERIES_COLUMN_ID)
    id2 = model.get_value(iter2, PublisherSeriesModel.PUBLISHER_SERIES_COLUMN_ID)
    if id1 == -1:
        return -1
    elif id2 == -1:
        return 1

    str1 = model.get_value(iter1, PublisherSeriesModel.PUBLISHER_SERIES_COLUMN_NAME)
    str2 = model.get_value(iter2, PublisherSeriesModel.PUBLISHER_SERIES_COLUMN_NAME)

    if str1 < str2:
        return -1
    elif str1 > str2:
        return 1
    return 0

class PublisherSeriesModel (gtk.ListStore):
    PUBLISHER_SERIES_COLUMN_ID = 0
    PUBLISHER_SERIES_COLUMN_NAME = 1

    def __init__ (self, library):
        self.library = library
        gtk.ListStore.__init__ (self,
                                gobject.TYPE_INT,    # ID
                                gobject.TYPE_STRING) # NAME
        result = self.library.db.query ("SELECT publisher_series_id, name FROM publisher_series").getresult()
        iter = self.append()
        self.set (iter,
                  self.PUBLISHER_SERIES_COLUMN_ID, -1,
                  self.PUBLISHER_SERIES_COLUMN_NAME, 'None')
        for (id, name) in result:
            iter = self.append ()
            self.set (iter,
                      self.PUBLISHER_SERIES_COLUMN_ID, id,
                      self.PUBLISHER_SERIES_COLUMN_NAME, name)
        self.set_sort_column_id (self.PUBLISHER_SERIES_COLUMN_NAME, gtk.SORT_ASCENDING)
        self.set_sort_func (self.PUBLISHER_SERIES_COLUMN_NAME, sort_func)

    def add_publisher_series (self, publisher_series):
        iter = self.append()
        self.set (iter,
                  self.PUBLISHER_SERIES_COLUMN_ID, publisher_series._id,
                  self.PUBLISHER_SERIES_COLUMN_NAME, publisher_series.name)
        path = self.get_path (iter)
        return path



class SelectPublisherSeriesDialog:
    def __init__ (self, library, publisher_series, parent):
        self.library = library
        self.xml = gtk.glade.XML ('PublisherSeriesDialog.glade', 'publisher_series_select_dialog')
        self.xml.get_widget ('publisher_series_select_new_button').connect ('clicked', self.new_publisher_series)
        self.model = PublisherSeriesModel (self.library)
        tree_view = self.xml.get_widget ('publisher_series_select_tree_view')
        tree_view.set_model (self.model)
        column = gtk.TreeViewColumn ('Series', gtk.CellRendererText(), text=PublisherSeriesModel.PUBLISHER_SERIES_COLUMN_NAME)
        tree_view.append_column (column)
        tree_view.get_selection().connect ('changed', self.selection_changed)
        if parent:
            self.xml.get_widget ('publisher_series_select_dialog').set_transient_for (parent)

    def selection_changed (self, selection):
        (unused, row) = selection.get_selected ()

        if row:
            self.xml.get_widget ('ok_button').set_sensitive (True)
        else:
            self.xml.get_widget ('ok_button').set_sensitive (False)

    def run (self, publisher_series):
        dialog = self.xml.get_widget ('publisher_series_select_dialog')
        button = dialog.run ()
        if button == gtk.RESPONSE_OK:
            selection = self.xml.get_widget ('publisher_series_select_tree_view').get_selection ()
            (model, row) = selection.get_selected ()
            id = model[row][0]
            if id > 0:
                publisher_series = self.library.get_object ('Library.PublisherSeries', id)
            else:
                publisher_series = None
        dialog.destroy ()
        return publisher_series

    def new_publisher_series (self, button):
        dialog = PublisherSeriesDialog (self.library, self.xml.get_widget ('publisher_series_select_dialog'))
        publisher_series = dialog.run ()
        if publisher_series:
            path = self.model.add_publisher_series (publisher_series)
            selection = self.xml.get_widget ('publisher_series_select_tree_view').get_selection ()
            selection.select_path (path)

class PublisherSeriesWidget:
    def __init__ (self, library, label, button, publisher_series=None, parent=None):
        self.library = library
        self.library = library
        self.label = label
        self.button = button
        self.parent = parent
        self.button.connect ('clicked', self.select_publisher_series)
        self.set_object (publisher_series)

    def select_publisher_series (self, button):
        dialog = SelectPublisherSeriesDialog (self.library, self.publisher_series, self.parent)
        publisher_series = dialog.run (self.publisher_series)
        self.set_object (publisher_series)

    def set_object (self, publisher_series):
        self.publisher_series = publisher_series
        if publisher_series:
            self.label.set_text (publisher_series.name)
        else:
            self.label.set_text ('No publisher series selected')

    def get_object (self):
        return self.publisher_series


class PublisherSeriesDialog:
    def __init__ (self, library, parent=None):
        self.library = library
        self.xml = gtk.glade.XML ('PublisherSeriesDialog.glade', 'publisher_series_dialog')
        if parent:
            self.xml.get_widget ('publisher_series_dialog').set_transient_for (parent)
        self.publisher = PublisherWidget (self.library,
                                          self.xml.get_widget ('publisher_series_publisher_omenu'))

    def run (self, publisher_series=None):
        dialog = self.xml.get_widget ('publisher_series_dialog')
        if publisher_series == None:
            self.setup ()
            dialog.set_title ("Create a new series")
        else:
            self.hydrate (publisher_series)
            dialog.set_title ("Edit existing series")

        while 1:
            button = dialog.run ()
            if button == gtk.RESPONSE_OK:
                try:
                    self.test ()
                except TestError, msg:
                    print msg
                    continue
                publisher_series = self.dehydrate (publisher_series)
                dialog.destroy ()
                return publisher_series
            else:
                dialog.destroy ()
                return None

    def test (self):
        if not self.xml.get_widget ('publisher_series_name_entry').get_text ():
            raise TestError ("Series must have a name")

    def setup (self):
        self.xml.get_widget ('publisher_series_name_entry').set_text ('')
        self.xml.get_widget ('publisher_series_notes_text_view').get_buffer ().set_text ('')
        
    def hydrate (self, publisher_series):
        self.xml.get_widget ('publisher_series_name_entry').set_text (publisher_series.name)
        if publisher_series.notes: self.xml.get_widget ('publisher_series_notes_text_view').get_buffer ().set_text (publisher_series.notes)
        self.publisher.set_object (publisher_series.publisher)

    def dehydrate (self, publisher_series):
        name = self.xml.get_widget ('publisher_series_name_entry').get_text ()
        buffer = self.xml.get_widget ('publisher_series_notes_text_view').get_buffer ()
        notes = buffer.get_text (buffer.get_start_iter(), buffer.get_end_iter())

        if publisher_series:
            publisher_series.name = name
            publisher_series.notes = notes
        else:
            kwargs = {}
            kwargs['name'] = name
            if notes: kwargs['notes'] = notes
            query = "SELECT publisher_series_id FROM publisher_series WHERE name=\'%s\'" % util.escape_string (name)
            result = self.library.db.query (query).getresult()
            if len (result) > 0:
                raise 'DuplicateError'
            publisher_series = self.library.create_object ('Library.PublisherSeries', **kwargs)
        publisher_series.publisher = self.publisher.get_object ()
        return publisher_series

if __name__ == '__main__':
    import sys
    import signal
    signal.signal (signal.SIGINT, signal.SIG_DFL)
    library = Library.Library()
    library.connect()
    if len (sys.argv) == 2:
        id = sys.argv[1]
        publisher_series = library.get_object ("Library.PublisherSeries", id)
    else:
        id = None
        publisher_series = None
    dialog = PublisherSeriesDialog (library)
    publisher_series = dialog.run (publisher_series)
    if publisher_series:
        print publisher_series._id
    
